========================================================================== <br />
 <br />
**Rutherford Backscattering Simulation in Arbitrary Defective Crystals** (**RBSADEC**)<br />
is a code that can simulate the signal of Rutherford backscattering spectrometry in<br />
channeling mode (RBS/C) from targets containing arbitrary atomic structures.<br />

The code can perform **parallel computing** with good performance. <br />
<br />
 **Originally developed/contributed by:** <br />
 	Shuo Zhang (Lanzhou University, China) <br />
	Kai Nordlund (University of Helsinki, Finland) <br />
	Flyura Djurabekova (University of Helsinki, Finland) <br />
	... <br />
 <br />
 **Further improved/contributed by:** <br />
    Xin Jin (University of Helsinki, Finland, 2021-Present) <br />
	Xin Jin (Université de Limoges & Université Paris-saclay, France, 2017-2021) <br />
	Jean-Paul Crocombette (Université Paris-saclay, CEA, France) <br />
	Aurélien Debelle (Université Paris-saclay, France) <br />
	... <br />
 <br />
 For [How to build and use](https://gitlab.com/xinJin/rbsadec/-/wikis/How-to-build-and-use), please check the wiki pages. <br />
 <br />
 If you have **questions**, please **ask** by posting an [Issue](https://gitlab.com/xinJin/rbsadec/-/issues/?sort=created_date&state=all&first_page_size=20). <br />
 I will check the questions time to time. <br />
 You may also find answers from questions posted in previous issues. <br />
 (A basic understanding on RBS/c and computer simulations can solve most of the problem :-)) <br />
 <br />
 _This code cannot generate simulation targets. _<br />
 <br />
========================================================================== <br />
 <br />
 **For citing the current version of the code:** <br />
 <br />
 S. Zhang, K. Nordlund, F. Djurabekova, Y. Zhang, G. Velisa, T. S. Wang,   <br />
 Simulation of Rutherford backscattering spectrometry from arbitrary atom  <br />
 structures, Phys. Rev. E [**94** (2016) 043319](https://doi.org/10.1103/PhysRevE.94.043319). <br />
 <br />
 X. Jin, J.-P. Crocombette, F. Djurabekova, S. Zhang, K. Nordlund, F. Gar- <br />
 rido, A. Debelle, New developments in the simulation of Rutherford backs- <br />
 cattering spectrometry in channeling mode using arbitrary atom structures <br />
 , Model. Simul. Mat. Sci. Eng. [**28** (2020) 075005](https://doi.org/10.1088/1361-651X/ab81a9). <br />
 <br />
 **Description of new features:** <br />
 - Improvement for the movement of probing ions in targets containing lattice voids <br />
 X. Jin, _et al._, J. Phys. D: Appl. Phys. [**56** (2023) 065303](https://doi.org/10.1088/1361-6463/acad12). <br />
 - Nuclear reaction analysis in channeling mode (**NRA/c**) <br />
 X. Jin, _et al._, Phys. Rev. Materials [8 (2024) 043604](https://doi.org/10.1103/PhysRevMaterials.8.043604). <br />

 ========================================================================== <br />

 **Other contributions**<br />

 Miguel Sequeira and Katharina Lorenz: Idea and discussion about the effect of atomic density on the electronic stopping power.
========================================================================== <br />
<br />
**Parallel computing**<br />
- Source code: [./Source](https://gitlab.com/xinJin/rbsadec/-/tree/master/Source)

**Serial computing**<br />
- Source code: [./Serial](https://gitlab.com/xinJin/rbsadec/-/tree/master/Serial)

**What's new** <br />

- Current verison [1.44]:<br />
Added a new feature: nuclear reaction analysis in channeling mode

- Version [1.41](https://gitlab.com/xinJin/rbsadec/-/tree/v.m.1.41/mpid):<br />
Take into account the effect of target atomic density variation on the movement of probing ions.<br />

- Version [1.40](https://gitlab.com/xinJin/rbsadec/-/tree/v.m.1.40):<br />
The output of ```final_xy.dat``` can make use of parallel computing.

- Version [1.38](https://gitlab.com/xinJin/rbsadec/-/tree/v.m.1.38):<br />
The output of ```rbsspectra.dat``` and ```final_direction.dat``` can make use of parrallel computing.

- Version [1.34](https://gitlab.com/xinJin/rbsadec/-/tree/v.mpid.1.34):<br />
From this version, the code is switched from serial to parallel computing.

- Version [1.32](https://gitlab.com/xinJin/rbsadec/-/tree/v1.32):<br />
This version consumes less memory. 

- Version [1.30](https://gitlab.com/xinJin/rbsadec/-/tree/v1.30):<br />
Added a new rotation mode ```Mode.rotate = 2```. 


- Version [1.28](https://gitlab.com/xinJin/rbsadec/-/tree/v1.28):<br />
Added a new option ```-loc2```. 


- Version [1.1](https://gitlab.com/xinJin/rbsadec/-/tree/v1.1):<br />
Increase the precision of the polar angle in the output file of ```final\_direction.dat```.
 
