#ifndef REBUILD_ATOM_TYPE_H
#define REBUILD_ATOM_TYPE_H

#include <string>
#include <vector>

#include "pointer.h"

#define MAXTYPE 92

namespace RBSsimuNS {
    class Atom_type : public Pointer {
    public:
        Atom_type(RBSsimu *rbs);

        std::unique_ptr<std::string[]>      symbol;
        std::unique_ptr<double[]>           z;
        std::unique_ptr<double[]>           mass;
        std::unique_ptr<double[]>           estop;
        std::unique_ptr<double[]>           edisplacement;

        std::string                         TablePath;

        void Atom_type_read();
        int value_atom_type(const std::string v_symbol, double &v_z, double &v_mass, double &v_edis, double &v_estop);

        ~Atom_type();
    };
}

#endif 
