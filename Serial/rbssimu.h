#ifndef REBUILD_RBSSIMU_H
#define REBUILD_RBSSIMU_H

#include <memory>
#include <vector>

#define   MAXRANDLIST     1000003
#define   MAXLOGLIST      104729
#define   MAXGAUSSLITST   1000003

namespace RBSsimuNS{
    class RBSsimu {
    public:
        RBSsimu();
        ~RBSsimu();

        std::shared_ptr<class Incident>     incident;
        std::shared_ptr<class Parameter>    parameter;
        std::shared_ptr<class Atom_type>    atom_type;
        std::shared_ptr<class Target>       target;
        std::shared_ptr<class Atom>         atom;

        int     seeduni, seedgauss, seedlog;
        int     gener;
        double  xr[3], yr[3], energy, weight;
        double  sigmach;
        int     countflag;
        int     depthnum;
        double  depthstep;

        std::vector<double>     randomlist, gaussianrandlist, lograndlist;
        std::vector<int>        channel;
        std::vector<double>     yield, depth, gencount;
        double  DechannelAngle; //AbJ
        std::vector<double>     Chyield, DeChyield; //AbJ
        double  detectortheta, detectorfii; //AbJ

        int     ffp_mode; //AbJ
        int     impact_mode; //AbJ
        int     TBM_mode; //AbJ

        void init();
        double randomx();
        void   randomizelist(std::vector<double> &list, unsigned int maxlist);
        void   Unirandlist();
        void   Gaussianlist();
        void   Lograndlist();
        double rand();
        double gaussianrand();
        double logrand();

        void emit_directions();
        void emit();
        void emit_energy(const double &z2, const double &m2, const double &cosangle);
        int  amorphous_slowdown();
        int  Detected();
        void getsignal();
        void printrbs(const int &num);
        void printdepthrbsnum(const int &num);

        void depthrbsnumber();

        void ele_ener_variation_FFP(double ffp, const double bin, double &en, const double mass1,
                                    const int locflag, const int binCount, const int locflag2, const double locDen); //AbJ
        void FFP_cutted_by_surface(double &ffp, double &px, double &py, double &pz, const double l, const double m, const double n, const double xlo2);//AbJ
        void scatter_angle(double &en, const double mass1, const double mass2,
                           const double factor, const double au, const double p,
                           double &l, double &m, double &n);
        int den_bin_select(const double px, const double py, const double pz);
    };
}



#endif 
