#include <iostream>
#include <string>
#include <chrono> 
#include <ctime> 
#include <cstdio> 
#include <memory> 

#include "rbssimu.h"
#include "incident.h"
#include "parameter.h"
#include "atom_type.h"
#include "target.h"
#include "atom.h"

using namespace RBSsimuNS;

int main(int argc, char **argv) {

    int i;
    int rangeflag, rbsflag, rbscountflag, nuclossflag, recoilspectraflag, finalangleflag, penetrationpowerflag;
    int pathflag, locDenflag, locDenflag2; //Abj

    std::string arg;

    rangeflag = 0;
    rbsflag = 0;
    rbscountflag = 0;
    nuclossflag = 0;
    recoilspectraflag = 0;
    finalangleflag = 0;
    penetrationpowerflag = 0;
    pathflag = 0;
    locDenflag = 0;
    locDenflag2 = 0;

    for (i = 1; i < argc; i++) {
        arg = argv[i];
        if (arg == "-h") {

            std::cerr << "Usage: " << argv[0] << " option(s) "
                      << std::endl 
                      << "Options: " << std::endl
                      << "-h          help\n"
                      << "-r          Range simulation\n"
                      << "-rbs        Rutherford Backscattering Spectra simulation \n"
                      << "-loc        Take into account the local density variation (elec. energy loss) \n"
                      << "-loc2       Similar to '-loc', but works in fixed regions (to be developed) \n"
                      << "-p          Output the encounter probabiltiy\n"
                      << "-nuc        Output the depth profile of nuclear energy loss \n"
                      << "-recoils    Output the recoil energy spectra \n"
                      << "-fa         Output the final angle distribution \n"
                      << "-pp         Output the penetration power of ions \n"
                      << "-path       Output the trajectory of ions (to be developed)\n"
                      << "Default:    -rbs" << std::endl;

            exit(0);
        }
        if (arg == "-r") rangeflag = 1;
        if (arg == "-rbs") rbsflag = 1;
        if (arg == "-p") rbscountflag = 1;
        if (arg == "-nuc") nuclossflag = 1;
        if (arg == "-recoils") recoilspectraflag = 1;
        if (arg == "-fa") finalangleflag = 1;
        if (arg == "-pp") penetrationpowerflag = 1;
        if (arg == "-path") pathflag = 1; //Abj
        if (arg == "-loc") locDenflag = 1; //Abj
        if (arg == "-loc2") locDenflag2 = 1; //AbJ
    }
    if (1 == argc) rbsflag = 1;

    std::unique_ptr<RBSsimu> rbssimu(new RBSsimu);

    rbssimu -> incident -> simu(rangeflag, rbsflag, rbscountflag, nuclossflag, recoilspectraflag, finalangleflag,
                                penetrationpowerflag, pathflag, locDenflag, locDenflag2);

    //Added by JIN
    auto end = std::chrono::system_clock::now();
    std::time_t end_time = std::chrono::system_clock::to_time_t(end);
    std::cout<<"\nComputation finished at: "<<std::ctime(&end_time)<<std::endl;
}
