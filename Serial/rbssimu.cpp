#include <cstdio>
#include <math.h> 
#include <fstream>
#include <iomanip>

#include "rbssimu.h"
#include "incident.h"
#include "parameter.h"
#include "atom_type.h"
#include "target.h"
#include "atom.h"

using namespace RBSsimuNS;

RBSsimu::RBSsimu() {

    incident.reset(new Incident(this));
    parameter.reset(new Parameter(this));
    atom_type.reset(new Atom_type(this));
    target.reset(new Target(this));
    atom.reset(new Atom(this));
}

RBSsimu::~RBSsimu() {
    
}

void RBSsimu::init() {
    int i;
    int nchannel = parameter -> nchannel;
    double sigma = parameter -> detectorfwhm / 2.355;
    double slope = parameter -> detectorslope;
    double size = parameter -> depthsize;

    countflag = 0;
    depthnum = 300;
    randomlist.resize(MAXRANDLIST, 0);
    gaussianrandlist.resize(MAXRANDLIST, 0);
    lograndlist.resize(MAXLOGLIST, 0);
    channel.resize(nchannel, 0);
    yield.resize(nchannel, 0);
    Chyield.resize(nchannel, 0);
    DeChyield.resize(nchannel, 0);
    depth.resize(depthnum, 0);
    gencount.resize(depthnum, 0);
    Unirandlist();
    Lograndlist();
    Gaussianlist();
    if(parameter -> seed < 0) { seeduni = 0; seedgauss = 0; seedlog = 0; }
    seeduni = parameter -> seed % MAXRANDLIST;
    seedgauss = parameter -> seed % MAXGAUSSLITST;
    seedlog = parameter -> seed % MAXLOGLIST;

    if(seeduni < 0 || seedgauss < 0 || seedlog < 0){ printf("seed number should be larger than zero\n"); exit(1);}

    sigmach = sigma / slope;
    for (i = 0; i < nchannel; i++) { channel[i] = i;}

    depthstep = size / depthnum;
    for (i = 0; i < depthnum; i++) { depth[i] = i * depthstep;}
}

double RBSsimu::randomx()
{
    static int seed1=39419293,seed2=9315294;
    int z, k;
    k = seed1 / 53668;
    seed1 = 40014 * (seed1 - k * 53668) - k * 12211;
    if (seed1 < 0) seed1 = seed1 + 2147483563;
    k = seed2 / 52774;
    seed2 = 40692 * (seed2 - k * 52774) - k * 3791;
    if (seed2 < 0) seed2 = seed2 + 2147483399;
    z = seed1 - seed2;
    if (z < 1) z = z + 2147483562;
    return z * 4.65661305956E-10;
}

void RBSsimu::randomizelist(std::vector<double> &list, unsigned int maxlist)
{
    unsigned int i, ilist, j;
    double x;
    for(i = 0; i < maxlist * 3; i++)
    {
        ilist = i % maxlist;
        j = (unsigned int) floor (randomx() * maxlist);
        x = list[ilist];
        list[ilist] = list[j];
        list[j] = x;//change the content between list[ilist] and list[j]
    }
}

void RBSsimu::Unirandlist()//ranges from 4.99999e-07 to 1
{
    int i;
    for(i = 0; i < MAXRANDLIST; i++) randomlist[i] = (i + 0.5) * 1.0 / MAXRANDLIST;
    randomizelist(randomlist, MAXRANDLIST);
}

void RBSsimu::Lograndlist()//ranges from 12.253 to 4.77424e-06
{
    int i;
    for(i = 0; i < MAXLOGLIST; i++) lograndlist[i] = -log((i + 0.5) * 1.0 /MAXLOGLIST);
    randomizelist(lograndlist,MAXLOGLIST);
}

void RBSsimu::Gaussianlist()
{
    int i, j = 0;
    double x;
    double a,b;

    for(i = 0; i < MAXGAUSSLITST; i++){
        a = sqrt(-2.0 * log(randomlist[j++]));//ranges from 5.38677 to 0.000999999
        if(j >= MAXRANDLIST) j = j - MAXRANDLIST;
        b = cos(2.0 * PI * randomlist[j++]);//ranges from -1 to 1
        if(j >= MAXRANDLIST) j = j - MAXRANDLIST;
        gaussianrandlist[i] = a * b;//ranges from -5.38677 to 5.38677
    }
}

double RBSsimu::rand()
{
    double x = randomlist[seeduni++];
    if(seeduni >= MAXRANDLIST) seeduni = seeduni - MAXRANDLIST;
    return x;
}
double RBSsimu::gaussianrand()
{
    double x = gaussianrandlist[seedgauss++];
    if(seedgauss >= MAXGAUSSLITST) seedgauss = seedgauss - MAXGAUSSLITST;
    return x;
}
double RBSsimu::logrand()
{

    double x = lograndlist[seedlog++];
    if(seedlog >= MAXLOGLIST) seedlog = seedlog - MAXLOGLIST;
    return x;
}

void RBSsimu::emit() {
    /*
     Calculate the encounter probability according to the impact parameter
     and the thermal vibration magnitude;
     Choose the backscattered direction;
     Calculate the backscattered energy and Rutherford backscattering differential cross section (weight);
     Simulate the amorphous slowing-down process;
     Calculate the yield in the detector.
     */

    int i, j, k, flag;
    const int *image = incident -> image;
    const double *xin = incident -> x;
    const double *y = incident -> y;
    const double *y0 = incident -> y_init;

    const std::vector<int> &itype = atom -> itype;
    const std::vector<double> &az = atom -> z;
    const std::vector<double> &amass = atom -> mass;
    //const std::vector<std::vector<double> > &xat = atom -> x; //Keyword: vector_memory
    double **xat = atom -> x;
    const std::vector<double> &debyemagn = atom -> debyemagn;

    const int &collnum = target -> collnum;
    const int *pbc = target -> pbc;
    const std::vector<int> &collindex = target -> collindex;
    const double *prd = target -> prd;

    double x0[3], tt, R[3], r2, sd, proba, mass2, z2;

    double cosemit;

    for (i = 0; i < collnum; i++){

        gener = 0;

        for (j = 0; j < 3; j++) x0[j] = xat[collindex[i]][j] - xin[j];
        //When the periodic boundary condition is applied,
        //and the distance is larger than half box size,
        //then do this.
        //Do not understand this part...
        for (j = 0; j < 3; j++)  {
            if (pbc[j] && x0[j] >   prd[j] / 2.0) x0[j] = x0[j] - prd[j];
            if (pbc[j] && x0[j] < - prd[j] / 2.0) x0[j] = x0[j] + prd[j];
        }
        tt = 0.0;
        for (j = 0; j < 3; j++) tt = tt + x0[j] * y[j];//the projection distance along the incident direction
        for (j = 0; j < 3; j++) R[j] = x0[j] - y[j] * tt;//the vector of impact parameter
        r2 = 0.0;
        for (j = 0; j < 3; j++) r2 = r2 + R[j] * R[j];//(the impact parameter modulus)**2

        if (r2 != r2) {
            printf("rbs ions collision wrong, see %s line %d \n", __FILE__, __LINE__);
            exit(1);
        }

        sd = debyemagn[itype[collindex[i]]];
        proba = 1.0 / (2.0 * PI * sd * sd) * exp(- r2 / (2.0 * sd * sd));//Encounter probability, 2D normal distribution
        while (proba > 1.0) {
            proba = proba - 1.0;
            gener++;
        }
        if (proba > rand()) gener++;

        if(countflag) depthrbsnumber();

        z2 = az[itype[collindex[i]]];
        mass2 = amass[itype[collindex[i]]];

        for (j = 0; j < gener; j++){
            energy = 0.0;
            for (k = 0; k < 3; k++) xr[k] = xin[k] + image[k] * prd[k];
            emit_directions();

            cosemit = 0.0;
            /*
             Why is the initial incident direction y_init(y0) used
             instead of the real-time moving direction y[k]?
             */
            for (k = 0; k < 3; k++) cosemit = cosemit + y0[k] * yr[k]; //angle between the initial incident direction and backscattered direction
            emit_energy(z2, mass2, cosemit);
            flag = amorphous_slowdown();
            if (flag == 0) continue;
            flag = Detected();
            if (flag == 0) continue;
            getsignal();
        }
    }
}

void RBSsimu::emit_directions(){
    /*
     Randomly (Uniformly) choose the backscattered ion direction
     inside a certain solid angle region decided by
     the spread angle.
     */

    const double dtheta = detectortheta / 180.0 * PI;
    const double dfii = detectorfii / 180.0 * PI;

    double l, m, n, fii;
    const double spread = parameter -> spreada / 180.0 * PI;
    double ct, st, cf, sf, k;

    l = sin(dtheta) * cos(dfii);
    m = sin(dtheta) * sin(dfii);
    n = cos(dtheta);

    fii = 2.0 * PI * rand();
    ct = 1.0 - rand() * (1.0 - cos(spread));//ct = cos(theta);
    st = sqrt(1.0 - ct * ct); //st = sin(theta);
    if(std::isnan(st)) {
        printf("The setting for spread angle has problem! See %s line %d \n", __FILE__, __LINE__);
        exit(1);
    }
    cf = cos(fii);
    sf = sin(fii);
    k = sqrt(1.0 - n * n);

    //Modified by JIN
    /*
     Original:
     yr[0] = l * ct + st / k * (l * n * cf + m * sf);
     yr[1] = m * ct + st / k * (m * n * cf - l * sf);
     yr[2] = n * ct - k * st * cf;
     */
    if (k != 0){
        yr[0] = l * ct + st / k * (l * n * cf - m * sf);
        yr[1] = m * ct + st / k * (m * n * cf + l * sf);
        yr[2] = n * ct - k * st * cf;
    }
    else {
        yr[0] = st * cf;
        yr[1] = st * sf;
        yr[2] = ct;
    }
}

void RBSsimu::emit_energy(const double &z2, const double &m2, const double &cosangle) {
    /*
     Calculate the backscattered energy
     and Rutherford backscattering differential cross section
     */

    double t1, sqfactor;
    double a1, a2, a3;

    const double &z1 = incident -> z;
    const double &m1 = incident -> mass;
    const double &ein = incident -> energy;

    t1 = m2 * m2 - m1 * m1 * (1.0 - cosangle * cosangle);
    if (t1 < 0.0) { printf ("warning: rbs weight calculation failed, see %s line %d\n", __FILE__, __LINE__); t1 = 0.0; }

    if (m1 < m2) sqfactor = (m1 * cosangle + sqrt(t1)) / (m1 + m2);//sqrt(kinematical factor)
    else sqfactor = (m1 * cosangle - sqrt(t1)) / (m1 + m2);

    energy = ein * sqfactor * sqfactor;

    a1 = pow(z1 * z2 * E2 / (2.0 * ein * (1.0 - cosangle * cosangle)), 2.0);
    a2 = pow(cosangle + sqrt(1.0 - m1 * m1 / m2 / m2 * (1.0 - cosangle * cosangle)), 2.0);
    a3 = sqrt(1.0 - m1 * m1 / m2 / m2 * (1.0 - cosangle * cosangle));
    weight = a1 * a2 / a3;
}

int RBSsimu::amorphous_slowdown() {
    /*
     Perform the simulation on the backscattered ion
     amorphous slowing-down

     New things (AbJ)
     Free Flight Path (FFP) mode:
     if 0, follow a Poisson process with the expected value as 10Rmax (original method);
     if 1, fixed distance;
     if 2, use the TRIM method (5 degree deflection per path length)

     Impact parameter selection mode:
     if 0, select according to the method used in the modified fixed volume method
     if 1, select according to the method used in the TRIM
     if 2, select according to the original method
     Note:
     If the FFP is smaller than the mean atomic separation,
     the impact parameter is selected according to another TRIM method
     */

    int impacindex,epsiindex;
    double k, l, m, n, ll, mm, nn, px, py, pz;
    double flightlength, loss, p, preduce, vv;
    double ecm, au, factor, epsilon, s2, ct, st, scatangle, T;
    double omega, sinomega, cosomega, sintheta, costheta;

    const double &rmax = target -> rmax;
    const double &z1 = incident -> z;
    const double &mass1 = incident -> mass;
    const double &estop = incident -> estop;

    const int &ntype = atom -> ntype;
    const std::vector<int> &itypen = atom -> itypenum;//total atom number for each element
    const int &natotal = atom -> natotal;

    const std::vector<double> &amass = atom -> mass;
    const std::vector<double> &az = atom -> z;
    const std::vector<std::vector<double> > &matrix = atom -> scat_matrix;

    const int *pbc = target -> pbc;
    const double *prd = target -> prd;

    double en, z2, mass2;
    double e = 1.6021892e-19;
    double u = 1.6605655e-27;

    int i;
    double t, ran;
    int flag = 0;

    /*AbJ*/
    double Natom, Natom2;
    double meanau, meanepsi, meanfactor, meanecm;
    const double &meanm = atom->meanmass;
    const double &meanz = atom->meanz;
    const double &depth_bin = parameter->freedist;
    const double *xlo = target -> xlo;
    const int locDenflag = parameter -> locDenflag;
    const int locDenflag2 = parameter -> locDenflag2;
    int binCount = -1;
    double locDen, locADen; //Abj

    l = yr[0];
    m = yr[1];
    n = yr[2];

    px = xr[0];
    py = xr[1];
    pz = xr[2];

    en = energy;

    Natom = 1 /(4 * PI * rmax * rmax * rmax/3); //AbJ

    while (en > estop){

        /*
         Modified b J.
         If the backscatterd depth is smaller than the target surface depth,
         the projectile directly leaves from the target,
         no more amorphous slowing-down
         */

        if (pz <= xlo[2]){
            flag = 1;
            xr[0] = px;
            xr[1] = py;
            xr[2] = pz;
            yr[0] = l;
            yr[1] = m;
            yr[2] = n;
            energy = en;
            break;
        }

        t = 0.0;
        i = 0;
        ran = rand();

        //Randomly choose the collision atom type
        while(1){
            t = t + (double) (itypen[i]) / (double) (natotal);
            if (t >= ran) break;
            else i++;
            if (i >= ntype) { printf("error. see %s %d, probability %lf, random number %lf\n", __FILE__, __LINE__, t, ran); exit(1);}
        }

        if(0 == ffp_mode){
            flightlength = rmax * 10.0 * logrand();
        }
        else if(1 == ffp_mode){
            flightlength = depth_bin;
        }
        else if(2 == ffp_mode){
            meanau = 0.8853 / (pow(z1, 0.23) + pow(meanz, 0.23)) * 0.52917721092;//mean universal screening length
            meanfactor = meanau / (z1 * meanz * E2);
            meanecm = en / (mass1 / meanm + 1.0);
            meanepsi = meanfactor * meanecm;

            flightlength = (0.02*pow(1+mass1/meanm,2) / (4*PI*meanau*meanau*Natom)) * (meanepsi*meanepsi + 0.052*pow(meanepsi,1.32)) / log(1 + meanepsi);
        }
        else{
            flightlength = rmax * 10.0 * logrand();
        }

        mass2 = amass[i];
        z2 = az[i];
        au = 0.8853 / (pow(z1, 0.23) + pow(z2, 0.23)) * 0.52917721092;//Universal screening length
        factor = au / (z1 * z2 * E2);

        if (0 == locDenflag & 0 == locDenflag2) Natom2 = Natom;
        else if (1 == locDenflag && 0 == locDenflag2) {
            //Find the bin number where the current virtual ion is located
            binCount = den_bin_select(px, py, pz);
            //(Calculate the local value of Natom2 in the future)
            Natom2 = Natom;
        }
        else if (0 == locDenflag && 1 == locDenflag2){
            //Calculate the local value of Natom2 according to the ion position
            target -> loc2DensityCheck(px, py, pz, locDen, Natom2);
        }
        else if (locDenflag == 1 && locDenflag2 == 1) {
            printf("The value of locDenflag and locDenflag2 cannot be both 1.\n");
            exit(1);
        }
        else{
            printf("The value of locDenflag (%d) or locDenflag2 (%d) has problem.\n", locDenflag, locDenflag2);
            exit(1);
        }

        /*
         Modified by JIN:
         Descriminate the FFP according to the approximate mean atomic separation Natom**(1/3);
         Make sure the amorphous slowing down process stops at the target surface
         */
        if (flightlength >= pow(Natom,-1/3)){

            px = px + flightlength * l;
            py = py + flightlength * m;
            pz = pz + flightlength * n;


            if(pz > xlo[2]){

                //Calculate electronic energy loss
                ele_ener_variation_FFP(flightlength, depth_bin, en, mass1, locDenflag, binCount,
                                       locDenflag2, locDen);

                //Calculate the impact parameter
                //If p = -1, no nuclear collision
                if(0 == impact_mode){
                    if (0 == Natom2) p = -1;
                    else p = sqrt(rand() / (PI * Natom2 * flightlength));
                }
                else if(1 == impact_mode){
                    if (0 == Natom2) p = -1;
                    else p = sqrt(-1 * log(rand())/(PI * Natom2 * flightlength));
                }
                else if(2 == impact_mode){
                    //This mode should be abandoned
                    p = sqrt(rand() * rmax * rmax * rmax / (PI * flightlength));
                }
                else{
                    if (0 == Natom2) p = -1;
                    else p = sqrt(rand() / (PI * Natom2 * flightlength));
                }

                //Calculate nuclear energy loss and update moving direction
                scatter_angle(en, mass1, mass2, factor, au, p, l, m, n);

            }

            else{
                FFP_cutted_by_surface(flightlength, px, py, pz, l, m, n, xlo[2]);
                ele_ener_variation_FFP(flightlength, depth_bin, en, mass1, locDenflag, binCount, locDenflag2, locDen);
            }
        }

        else{

            flightlength = pow(Natom,-1/3);
            px = px + flightlength * l;
            py = py + flightlength * m;
            pz = pz + flightlength * n;

            if(pz > xlo[2]){
                ele_ener_variation_FFP(flightlength, depth_bin, en, mass1, locDenflag, binCount, locDenflag2, locDen);

                if (0 == Natom2) p = -1;
                else p = sqrt(rand()/(PI*pow(Natom,2/3)));

                //Calculate nuclear energy loss and update moving direction
                scatter_angle(en, mass1, mass2, factor, au, p, l, m, n);

            }

            else{
                FFP_cutted_by_surface(flightlength, px, py, pz, l, m, n, xlo[2]);
                ele_ener_variation_FFP(flightlength, depth_bin, en, mass1, locDenflag, binCount, locDenflag2, locDen);
            }
        }

        if (pz <= xlo[2]){
            flag = 1;
            xr[0] = px;
            xr[1] = py;
            xr[2] = pz;
            yr[0] = l;
            yr[1] = m;
            yr[2] = n;
            energy = en;
            break;
        }
        if (pbc[2] == 0 && pz > prd[2]) return 0;
    }

    return flag;
}

int RBSsimu::Detected() {
    /*
     Detect if the backscattered ion enters into the detector
     */

    int i;

    double ddetec[3], cosemit, cdetec;
    const double dtheta = detectortheta /180.0 * PI ;
    const double dfii = detectorfii / 180.0 * PI;
    const double &dist = parameter -> detectordistance;//[cm]
    const double &rad = parameter -> detectorradius;//[mm]

    ddetec[0] = sin(dtheta) * cos(dfii);
    ddetec[1] = sin(dtheta) * sin(dfii);
    ddetec[2] = cos(dtheta);
    cosemit = 0.0;

    for (i = 0; i < 3; i++) cosemit = cosemit + yr[i] * ddetec[i];//angle between the detector and the backscattered ion
    cdetec = dist * 10.0 / sqrt(rad * rad + dist *10.0 * dist * 10.0);
    if (cosemit >= cdetec) return 1;
    else return 0;
}

void RBSsimu::getsignal() {
    /*
     Calculate the yield in the detector channel

     New things (AbJ)
     Two Beam Method (TBD) mode:
     If 1,
     Distinguish the yield according to the incident ion polar angle;
     if the polar angle is no larger than "DechannelAngle", the yield comes from the channeled ion;
     if the polar angle is larger than "DechannelAngle", the yield comes from the dechanneled ion.
     */

    int i;
    double ch;
    const int &nchannel = parameter -> nchannel;
    const double &slope = parameter -> detectorslope;
    const double &intercept = parameter -> detectorintercept;

    const double *y = incident -> y; //AbJ

    ch = (energy / 1000.0 - intercept) / slope;

    for (i = 0; i < nchannel; i++){
        yield[i] = yield[i] + weight * 1.0 / (sigmach * sqrt(2.0 * PI)) * exp(- (channel[i] + 0.5 - ch) * (channel[i] + 0.5 - ch) / (2.0 * sigmach * sigmach));
    }

    if (1 == TBM_mode){
        const double ionangle = acos(y[2]) * 180 / PI;

        if (ionangle <= DechannelAngle){
            for (i = 0; i < nchannel; i++){
                Chyield[i] = Chyield[i] + weight * 1.0 / (sigmach * sqrt(2.0 * PI)) * exp(- (channel[i] + 0.5 - ch) * (channel[i] + 0.5 - ch) / (2.0 * sigmach * sigmach));
            }
        }
        else{
            for (i = 0; i < nchannel; i++){
                DeChyield[i] = DeChyield[i] + weight * 1.0 / (sigmach * sqrt(2.0 * PI)) * exp(- (channel[i] + 0.5 - ch) * (channel[i] + 0.5 - ch) / (2.0 * sigmach * sigmach));
            }
        }
    }
}

void RBSsimu::printrbs(const int &num) {
    /*
     Print the RBS/C yield in each channel
     Note:
     The yield actually should correspond to (channel + 0.5*bin)
     New things (AbJ)
     Two Beam Method (TBD) mode:
     If 1,
     print the yield from both dechanneled and channeled ions.
     */

    int i;
    int nprint = 40;
    const int &nchannel = parameter -> nchannel;
    std::ofstream rbsout;
    std::ofstream chrbsout, dechrbsout; //AbJ

    if ((num + 1) % nprint == 0 || (num + 1) == incident -> nincident){
        rbsout.open("out/rbsspectra.dat", std::ios::out);
        if (!rbsout){
            printf("Cannot open the out/rbsspectra.dat file\n");
            rbsout.close();
        }
        else{
            for (i = 0; i < nchannel; i++){
                rbsout << std::setw(10) << std::right << channel[i] << "  " << std::scientific << yield[i] << std::endl;
            }
            rbsout.close();
        }

        if (1 == TBM_mode){
            chrbsout.open("out/rbsspectra_ch.dat", std::ios::out);
            if (!chrbsout){
                printf("Cannot open the out/rbsspectra_ch.dat file\n");
                chrbsout.close();
            }
            else{
                for (i = 0; i < nchannel; i++){
                    chrbsout << std::setw(10) << std::right << channel[i] << "  " << std::scientific << Chyield[i] << std::endl;
                }
                chrbsout.close();
            }

            dechrbsout.open("out/rbsspectra_dech.dat", std::ios::out);
            if (!dechrbsout){
                printf("Cannot open the out/rbsspectra_dech.dat file\n");
                dechrbsout.close();
            }
            else{
                for (i = 0; i < nchannel; i++){
                    dechrbsout << std::setw(10) << std::right << channel[i] << "  " << std::scientific << DeChyield[i] << std::endl;
                }
                dechrbsout.close();
            }

        }
    }
}

void RBSsimu::depthrbsnumber(){
    /*
     Count the total number of backscattered collisions
     in a certain depth region:
     gencount[j]
     */

    int i, j;
    double x[3];
    const int *image = incident -> image;
    const double *xin = incident -> x;
    const double *prd = target -> prd;

    x[2] = xin[2] + prd[2] * image[2];

    j = x[2] / depthstep;
    if (j >= depthnum) j = depthnum - 1;
    gencount[j] = gencount[j] + gener;
}

void RBSsimu::printdepthrbsnum(const int &num) {
    /*
     Print the total backscattered event number in each depth bin.
     Note:
     The gencount actually should correspond to (depth + 0.5)
     */

    int i;
    int nprint = 40;

    std::ofstream  backout;

    if ((num + 1) % nprint == 0 || (num + 1) == incident -> nincident){
        backout.open("out/rbscount.dat", std::ios::out);
        if(!backout){
            printf("Cannot open the out/rbscount.dat file\n");
            backout.close();
        }
        else{
            for (i = 0; i < depthnum; i++){
                backout << std::setw(10) << std::fixed << depth[i] << "  " << std::setw(10) << gencount[i] << std::endl;
            }
            backout.close();
        }
    }
}

void RBSsimu::FFP_cutted_by_surface(double &ffp, double &px, double &py, double &pz, const double l, const double m,
                                    const double n, const double xlo2) {
    /*
     If after FFP, the projectile depth is smaller than the target surface depth,
     re-calculate the FFP in order to have the projectile located at the surface
     */

    //Go back to the last position
    px = px - ffp * l;
    py = py - ffp * m;
    pz = pz - ffp * n;

    //Calculate the new FFP
    ffp = fabs(ffp * ( (pz - xlo2 ) / ( ffp * n ) ) );

    //New position at the surface
    px = px + ffp * l;
    py = py + ffp * m;
    pz = xlo2;
}

int RBSsimu::den_bin_select(const double px, const double py, const double pz) {
    /*
     Find the bin number (den) of the current virtual ion
     */

    std::vector<int> idbin(3, 0);
    std::vector<double> x{px, py, pz};
    int binExceed = 0;
    int binCount = 0;
    const std::vector<double> xlo(target->xlo, target->xlo + 3);
    const std::vector<double> binsize_den = target->binsize_den;
    const std::vector<int> binnum_den = target->binnum_den;
    const std::vector<int> pbc(target->pbc, target->pbc + 3);

    for (int j = 0; j < 3; j++) {
        idbin.at(j) = (floor)((x[j] - xlo[j]) / binsize_den.at(j));

        if (idbin.at(j) < 0 && pbc[j] != 0) idbin.at(j) = (idbin.at(j) % binnum_den.at(j)) + binnum_den.at(j);
        if (idbin.at(j) >= binnum_den.at(j) && pbc[j] != 0) idbin.at(j) = idbin.at(j) % binnum_den.at(j);
        if (pbc[j] == 0 && idbin.at(j) < 0) binExceed = 1;
        if (pbc[j] == 0 && idbin.at(j) >= binnum_den.at(j)) binExceed = 1;
    }

    if (1 == binExceed) {
            /*
             If the pbc is 0, it is possible that the bin number would exceed the limit.
             A negative number will report this issue in the calculation of energy loss.
             */
            binCount = -1;
    }
    else {
            binCount = idbin.at(0) * (binnum_den.at(1) * binnum_den.at(2))
                       + idbin.at(1) * binnum_den.at(2) + idbin.at(2);

            if (binCount < 0 || binCount >= binnum_den[0] * binnum_den[1] * binnum_den[2]) {
                //Bound limit overflow happened here!
                printf("Warning, bound limit overflow, UB! see %s line %d \n", __FILE__, __LINE__);
                printf("Bin_DEN size (x, y, z): %.4f %.4f %.4f\n", binsize_den[0], binsize_den[1], binsize_den[2]);
                printf("Bin_DEN Number (x, y, z): %.d %.d %.d\n", binnum_den[0], binnum_den[1], binnum_den[2]);
                printf("Virtual ion position (x, y, z): %.4f %.4f %.4f\n", x[0], x[1], x[2]);
                printf("Bin_DEN position (x, y, z): %.d %.d %.d\n", idbin[0], idbin[1], idbin[2]);
                printf("Current bin number: %d\n", binCount);
                exit(1);
            }
    }

    return binCount;
}

void RBSsimu::ele_ener_variation_FFP(double ffp, const double bin, double &en, const double mass1,
                                     const int locflag, const int binCount, const int locflag2, const double locDen){
    /*
     Take into account the electronic energy loss
     variation in the FFP.
     Update the backscattered ion energy.
     */

    double vv;
    double loss;
    double e = 1.6021892e-19;
    double u = 1.6605655e-27;
    double denRatio = 1.0;
    const std::vector<double> &binDen = target -> binDen;

    if (0 == locflag && 0 == locflag2) denRatio = 1;
    else if (0 == locflag && 1 == locflag2) {
        //Check if the ion is in the empty zone
        //Empty zone: [1454.02, 1554.02) along the z axis
        //if (pz2 >= 1454.02 && pz2 < 1554.02) {
        //    denRatio = 0;
        //}
        //else denRatio = 1;

        denRatio = locDen / parameter -> density;
    }
    else if (1 == locflag && 0 == locflag2) {
        if (binCount < 0) denRatio = 1;
        else denRatio = binDen.at(binCount);
    }
    else if (locflag == 1 && locflag2 == 1) {
        printf("The value of locDenflag and locDenflag2 cannot be both 1.\n");
        exit(1);
    }
    else{
        printf("The value of locDenflag (%d) or locDenflag2 (%d) has problem.\n", locflag, locflag2);
        exit(1);
    }

    while(ffp > bin){
        vv = sqrt (2.0 * en * e / (mass1 * u));
        loss = incident -> eloss(vv) * denRatio;
        en = en - loss * bin;
        ffp = ffp - bin;
    }

    vv = sqrt (2.0 * en * e / (mass1 * u));
    loss = incident -> eloss(vv) * denRatio;
    en = en - loss * ffp;
}

void RBSsimu::scatter_angle(double &en, const double mass1, const double mass2, const double factor,
                            const double au, const double p, double &l, double &m, double &n) {
    /*
     Calculate the new moving direction after each elastic collision with target atoms
     (in the amorphous slowing down process)
     */

    int impacindex, epsiindex;
    double ecm, epsilon, preduce;
    double s2, ct, st, T;
    double scatangle, sintheta, costheta;
    double omega, cosomega, sinomega;
    double k, ll, mm, nn;
    const std::vector<std::vector<double> > &matrix = atom -> scat_matrix;

    //If atomic density iz zero, no nuclear collision event
    if (-1 == p) return;

    ecm = en / (mass1 / mass2 + 1.0);
    epsilon = factor * ecm;//Reduced energy
    preduce = p / au;//should be reduced distance

    impacindex = target -> sindex(target -> d2f(preduce));
    epsiindex = target -> eindex(target -> d2f(epsilon));
    if (epsiindex > DIME) printf("The energy used in Matrix is beyond DIME\n");
    s2 = matrix[epsiindex][impacindex];//sin(thetaCM)**2

    ct = 2.0 * (1.0 - s2) - 1.0;
    st = sqrt(1.0 - ct * ct);
    scatangle = atan(st / (ct + mass1 / mass2));

    if (scatangle < 0.0) scatangle = scatangle + PI;
    sintheta = sin(scatangle); costheta = sqrt(1.0 - sintheta * sintheta);
    T = 4.0 * mass1 * mass2 / ((mass1 + mass2) * (mass1 + mass2)) * en * s2;
    //!New energy
    en = en - T;
    omega = 2.0 * PI * rand();
    cosomega = cos(omega);
    sinomega = sqrt(1.0 - cosomega * cosomega);

    k = sqrt(1.0 - n * n);
    if (k == 0){
        ll = sintheta * cosomega;
        mm = sintheta * sinomega;
        nn = costheta;
    }
    else{
        //Modified by JIN
        /*
         Original:
         yr[0] = l * ct + st / k * (l * n * cf + m * sf);
         yr[1] = m * ct + st / k * (m * n * cf - l * sf);
         yr[2] = n * ct - k * st * cf;
         */
        ll = l * costheta + sintheta / k * (l * n * cosomega - m * sinomega);
        mm = m * costheta + sintheta / k * (m * n * cosomega + l * sinomega);
        nn = n * costheta - k * sintheta * cosomega;
    }

    //!New projection after scattering
    l = ll; m = mm; n = nn;
}
