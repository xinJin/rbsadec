#ifndef REBUILD_PARAMETER_H
#define REBUILD_PARAMETER_H

#include <memory>
#include <vector>

#include "pointer.h"

namespace RBSsimuNS {
    class Parameter : public Pointer {
    public:
        std::string                 ionsymbol;
        double                      energy;
        int                         numhis;
        double                      thetamin;
        double                      thetamax;
        double                      fiimin;
        double                      fiimax;
        double                      rotatheta;
        double                      rotafii;

        int                         ntype;
        double                      debyetemperature;
        int                         debyeflag;
        double                      density;
        std::vector<std::string>    atomsymbol;
        std::vector<double>         vibration; //Added by JIN

        double                      minipot;  //Added by JIN
        double                      coffset;  //Added by JIN
        double                      freedist; //Added by JIN
        double                      ThresholdEnergy; //Added by JIN

        double                      positionmin[3];
        double                      positionmax[3];
        double                      xlo[3];
        double                      boxsize[3];
        int                         boundary[3];
        double                      binsize[3];
        std::vector<double>         dbinsize;

        double                      temperature;
        double                      depthstop;
        int                         elstopmode;
        double                      spreada; //Added by JIN
        int                         seed;

        double                      detectortheta;
        double                      detectorfii;
        double                      detectorradius;
        double                      detectordistance;
        int                         nchannel;
        double                      detectorfwhm;
        double                      detectorslope;
        double                      detectorintercept;
        double                      depthsize;

        std::string                 inputpath; //Added by JIN
        int                         locDenflag; //Abj
        int                         locDenflag2; //Abj

        Parameter(RBSsimu *rbs);
        ~Parameter();

        void read_parameter();

        //readsc: read string
        //readic: read int
        //readicf: read int and print by scientific formatting
        //readdc: read double
        //readdcf: read double and print by scientific formatting
        void readsc(const std::string varname, std::string &v, const std::string v0, const std::string mess, std::ifstream &in);
        void readic(const std::string varname, int &v, const int v0, const std::string mess, std::ifstream &in);
        void readicf(const std::string varname, int &v, const int v0, const std::string mess, std::ifstream &in);
        void readdc(const std::string varname, double &v, const double v0, const std::string mess, std::ifstream &in);
        void readdcf(const std::string varname, double &v, const double v0, const std::string mess, std::ifstream &in);
        void readBox(const std::string varname, std::vector<double> &v, const std::vector<double> v0, const std::string mess, std::ifstream &in);
    };
}


#endif 
