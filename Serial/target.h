#ifndef REBUILD_TARGET_H
#define REBUILD_TARGET_H

#include <vector>

#include "pointer.h"

#define COMPUERRO 1e-4

#define MAXCOLLNUM 80
#define MAXCHECKNUM 100000

//*************************************************************************************************************
#define   FLT_MIN       -0.0000019073486328125f  //最小的溢出限= -2^(-19); overflow (exceeds the maximum or minimum) value the datatype have
#define   MINE          0.0000019073486328125f  //  minimum reduced energy = 2^(-19);in COM coordinates?
#define   MAXE          2097152.f // maximum reduced energy = 2^21
#define   DIME          640    // matrix dimension: (21-(-19))*2^4 (4 mantissa bits: *2^4)
#define   BIASE         1728   // (127-19)*2^4: exponent bias correction while performing division by 2^(-4)
#define   SHIFTE        19    // keep 4 of the 23 mantissa bits (23-4=19)

#define   MINS          0.00000001490116119384765625f // mimimum reduced impact parameter = 2^(-26)
#define   MAXS          64.f   // maximum reduced impact parameter = 2^6
#define   DIMS          512    // (6-(-26))*2^4
#define   BIASS         1616   // (127-26)*2^4: exponent bias correction while performing division by 2^(-16)
#define   SHIFTS        19    // keep 4 of the 23 mantissa bits (23-4=19)
//*************************************************************************************************************

#define   E2            14.3996445  //e*e/(4.0*pi eps0)  [eV*A]
#define   PI            3.14159265

namespace RBSsimuNS {
    class Target : public Pointer {
    public:

        double                  rmax;

        int                     nuclossflag;
        int                     nucdepthnum;
        int                     recoilspectraflag;
        int                     recspecsize;
        int                     maxrecsize;
        int                     drecspec;

        int                     lastcollnum;

        int                     pbc[3];
        double                  prd[3];
        double                  xlo[3], xhi[3];
        double                  binsize[3];
        double                  minimum_binsize;
        int                     nbinx, nbiny, nbinz;

        std::vector<int>        checkindex;
        std::vector<std::vector<double> >     debyedisp;
        std::vector<double>     depth;
        std::vector<double>     nucloss;
        std::vector<double>     ndisp;
        std::vector<double>     rece;
        std::vector<double>     irecspec;
        std::vector<double>     recspec;

        int                     pathflag; //AbJ (inactive)

        std::vector<int>        collindex;
        std::vector<double>     coll_dist;
        std::vector<double>     coll_para;
        std::vector<std::vector<double> >   coll_disp;
        std::vector<int>        lastcollindex;
        std::vector<double>     angle;
        std::vector<double>     nuclear_energy_loss;

        double                  depthstep;

        std::vector<int>        head;
        std::vector<int>        list;
        std::vector<double>     binDen; //Abj
        std::vector<double>     binsize_den; //Abj
        std::vector<int>        binnum_den; //Abj
        std::vector<double>     locBox;

        int                     checknum;
        int                     collnum;

        Target(RBSsimu *rbs);
        ~Target();

        void init();
        void bin();

        void find_check_atom();
        void find_coll_partner();
        void collision();

        /*
         Note:
         It looks like this function always return val,
         never return 0.0f
         */
        float d2f(double val){
            if (val < 0.0) return -val < FLT_MIN ? 0.0f : (float)val;
            return val < FLT_MIN ? 0.0f : (float)val;
        }
        unsigned int sindex(float val);
        unsigned int eindex(float val);

        void nuclosscal();
        void printdepthnuc(const int &num);

        void recoilspectra();
        void printrecoilspectra(const int &num);

        void loc2DensityCheck(double x, double y, double z, double &den, double &aDen); //Abj
    };
}


#endif 
