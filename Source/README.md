 <br />
 <div align="center">
<pre>Parallel computing</pre>
</div>

<br />
 For [How to build and use](https://gitlab.com/xinJin/rbsadec/-/wikis/How-to-build-and-use), please check the wiki pages. <br />
 <br />

The parallel computing is achieved by using message passing interface (MPI). <br />

Currently, the outputs of ```rbsspectra.dat```, ```nraspectra.dat```, ```final_direction.dat``` and ```final_xy.dat``` can take advantage of the parallel computing. For other outputs, please use only 1 processor.
 
