//
// Created by jinxinjx on 29/03/2022.
//

#include <math.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <mpi.h>

#include "nrasimu.h"
#include "rbssimu.h"
#include "incident.h"
#include "atom.h"
#include "atom_type.h"
#include "target.h"
#include "parameter.h"

using namespace RBSsimuNS;

int world_size;
int world_rank;

RBSsimuNS::NRA::NRA(RBSsimu *rbs):Pointer(rbs) {

}

RBSsimuNS::NRA::~NRA() {

}

void RBSsimuNS::NRA::init() {
    /***
     * Initialization.
     * - read the nuclear reaction cross section data;
     * - read the stopping power dedicated to the emitted nucleus;
     * - read the atomic number and the mass of the emitted and residual nucleus;
     * - read the reaction Q energy;
     */

    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    int i, j;
    double tmp1, tmp2;

    int nchannel = parameter -> nchannel;

    crossSecPath = parameter -> crossSecPath;
    emitStopPowerPath = parameter -> emitStopPowerPath;

    Qenergy = parameter -> nraQenergy;
    spreadAng = parameter -> nraSpreadAngle;

    reactionType = parameter -> nraReactionType;
    nucleusSymbol = parameter -> nucleusSymbol;
    emitNucSymbol = parameter -> emitNucluesSymbol;
    residualNucSymbol = parameter -> residualNucleusSymbol;

    emitNucZ.resize(1, 0);
    emitNucMass.resize(1, 0);
    residualNucZ.resize(1, 0);
    residualNucMass.resize(1, 0);
    NRAyield.resize(nchannel, 0);
    if (0 == world_rank) NRATotalYield.resize(nchannel, 0);

    for (i=0; i<1; i++) {
        j = atom_type -> value_atom_type(emitNucSymbol, emitNucZ[i], emitNucMass[i], tmp1, tmp2);
        if(j == 0) {
            if (0 == world_rank) printf("Error: cannot find the emitted nucleus %s, check !!\n", emitNucSymbol.c_str());
            exit(1);
        }

        j = atom_type -> value_atom_type(residualNucSymbol, residualNucZ[i], residualNucMass[i], tmp1, tmp2);
        if(j == 0) {
            if (0 == world_rank) printf("Error: cannot find the residual nucleus %s, check !!\n", residualNucSymbol.c_str());
            exit(1);
        }
    }

    //printf("Emit: %s %.0f %.3f\n", emitNucSymbol.c_str(), emitNucZ[0], emitNucMass[0]);
    //printf("Residual: %s %.0f %.3f\n", residualNucSymbol.c_str(), residualNucZ[0], residualNucMass[0]);
    readCS(crossSecPath, EnerCS, diffCS);
    read_ele_energyloss(emitStopPowerPath, v_loss, e_loss);
}

void RBSsimuNS::NRA::readCS(const std::string inputPath, std::vector<double> &ein, std::vector<double> &cs) {

    /***
     * Read the collision differential cross section from inputPath.
     * The first two lines will be ignored.
     * First column: energy [keV]
     * Second column: differential cross section [barn/steradian]
     *
     * Modified:
     *          ein [eV]
     *          cs [A^2/steradian]
     */

    int lineCount = 0;
    double tmp1, tmp2;
    std::string line;
    std::stringstream ss;
    std::ifstream op;

    op.open(inputPath);
    if (!op) {
        if (0 == world_rank) std::cerr << "Oh no," << inputPath <<  " could not be opened for reading!" << std::endl;
        exit(1);
    }

    while(op) {
        lineCount += 1;

        if (lineCount > 2) {
            ss.clear();
            getline(op, line);
            ss.str(line);
            if (ss >> tmp1 && ss >> tmp2) {
                ein.push_back(tmp1 * 1e3);
                cs.push_back(tmp2 * 1e-8);
            }
        }

    }
    op.close();
}

void RBSsimuNS::NRA::read_ele_energyloss(const std::string stopPath, std::vector<double> &v_loss, std::vector<double> &e_loss) {
    /**
     Read the (electronic) stopping power data dedicated to the emitted nucleus, similar to Incident::read_ele_energyloss();

     The first column is the velocity (m/s);
     The second column is the (electronic) stopping power (eV/Angstrom).
     */

    double tmp1, tmp2;
    std::string line;
    std::stringstream ss;
    std::ifstream readStop;

    readStop.open(stopPath);
    if(!readStop){
        std::cerr << "Oh no," << stopPath <<  " could not be opened for reading!" << std::endl;
        exit(1);
    }

    while(readStop){
        ss.clear();
        getline(readStop, line);
        ss.str(line);
        if(ss >> tmp1 && ss >> tmp2){
            v_loss.push_back(tmp1);
            e_loss.push_back(tmp2);
        }
    }
    readStop.close();
}

void RBSsimuNS::NRA::emit() {

    int i, j, k;
    int gener, flag;
    double z2, mass2, sd, xr[3], yr[3], cosemit, eout;
    double elasticCollFlag;

    const int *image = incident -> image;
    const double *xin = incident -> x;
    const double *y = incident -> y;
    const double *y0 = incident -> y_init;
    const double &z1 = incident -> z;
    const double &m1 = incident -> mass;
    const double &ein = incident -> energy;

    const std::vector<int> &itype = atom -> itype;
    const std::vector<double> &az = atom -> z;
    const std::vector<double> &amass = atom -> mass;
    double **xat = atom -> x;
    const std::vector<double> &debyemagn = atom -> debyemagn;
    const int &ntype = atom -> ntype;
    const std::vector<std::string> &atomSymbol = atom -> symbol;

    const int &collnum = target -> collnum;
    const int *pbc = target -> pbc;
    const std::vector<int> &collindex = target -> collindex;
    const double *prd = target -> prd;

    const double dtheta = rbssimu -> detectortheta / 180.0 * PI;
    const double dfii = rbssimu -> detectorfii / 180.0 * PI;

    const double &ddist = parameter -> detectordistance;//[cm]
    const double &dradi = parameter -> detectorradius;//[mm]
    const int &nchannel = parameter -> nchannel;
    const double &slope = parameter -> detectorslope;
    const double &intercept = parameter -> detectorintercept;

    const std::vector<int> channel = rbssimu -> channel;
    const double sigmach = rbssimu -> sigmach;

    for (i=0; i<collnum; i++) {

        if (atomSymbol[itype[collindex[i]]] != nucleusSymbol) continue;

        gener = 0;

        z2 = az[itype[collindex[i]]];
        mass2 = amass[itype[collindex[i]]];
        sd = debyemagn[itype[collindex[i]]];

        gener = virtualIon(xin, xat[collindex[i]], y, pbc, prd, sd);

        //printf("gener = %d\n", gener);

        //if (gener > 1) printf("gener = %d\n", gener);

        for (j = 0; j < gener; j++) {

            for (k = 0; k < 3; k++) xr[k] = xin[k] + image[k] * prd[k];
            emitDirections(dtheta, dfii, spreadAng, yr);

            cosemit = 0.0;
            for (k = 0; k < 3; k++) cosemit = cosemit + y0[k] * yr[k]; //angle between the initial incident direction and backscattered direction

            /***
             * Reaction type:
             * 0 - Elastic scattering, the scattering energy is calculated by elasticEner();
             * 1 - Nuclear reaction with a reaction Q energy, which is read from the input.
             */

            if (0 == reactionType) {
                //if (elasticCollFlag < 0), it is impossible to have a scattering event towards that direction
                elasticCollFlag = mass2 * mass2 - m1 * m1 * (1.0 - cosemit * cosemit);
                if (elasticCollFlag < 0) continue;
                else eout = elasticEner(m1, mass2, cosemit, ein);
            }
            else if (1 == reactionType) {
                eout = nuclearEner(m1, mass2, emitNucMass[0], residualNucMass[0], cosemit, ein, Qenergy);
                //printf("%.3f %.3f %.3f %.3f %.3f %.3e %.3e\n", m1, mass2, emitNucMass[0], residualNucMass[0], cosemit, ein, Qenergy);
                //printf("%.3e\n", eout);
                //exit(1);
            }
            else {
                if (0 == world_rank) {
                    printf("Error: nra->reaction type can only be 0 or 1, not %d\n", reactionType);
                    exit(1);
                }
            }

            weight = getCS(ein, EnerCS, diffCS);

            //printf("Before: Loc: %.3f %.3f %.3f; Ang: %.3f %.3f %.3f; En = %.3f\n", xr[0], xr[1], xr[2], yr[0], yr[1], yr[2], eout);
            flag = amorphous_slowdown(emitNucZ[0], emitNucMass[0], xr[0], xr[1], xr[2], yr[0], yr[1], yr[2], eout, v_loss, e_loss);
            //printf("After: Loc: %.3f %.3f %.3f; Ang: %.3f %.3f %.3f; En = %.3f, flag = %d\n", xr[0], xr[1], xr[2], yr[0], yr[1], yr[2], eout, flag);
            //exit(1);

            if (0 == flag) continue;
            flag = detected(yr, dtheta, dfii, ddist, dradi);
            if (0 == flag) continue;
            getSignal(NRAyield, eout, weight, nchannel, slope, intercept, sigmach, channel);
        }
    }
}

int RBSsimuNS::NRA::virtualIon(const double *xin, const double *xatom, const double *direc, const int *pbc,
                                     const double *sizes, const double debyeMagnit) {

    /***
     * Calculate the number of virtual ions according to the nuclear encounter probability.
     * xin: the coordinates of incident ion;
     * xatom: the coordinates of the target atom;
     * direc: the moving direction of incident ion (sin(theta) * cos(fii), sin(theta) * sin(fii), cos(theta));
     * pbc: periodic boundary conditions;
     * sizes: target box sizes;
     * debyeMagnit: target atom thermal vibration magnitude.
     *
     * Return:
     * The number of virtual ions.
     */

    int count = 0;
    int j;
    double x0[3], tt, R[3], r2, proba;

    for (j = 0; j < 3; j++) x0[j] = xatom[j] - xin[j];

    for (j = 0; j < 3; j++)  {
        if (pbc[j] && x0[j] >   sizes[j] / 2.0) x0[j] = x0[j] - sizes[j];
        if (pbc[j] && x0[j] < - sizes[j] / 2.0) x0[j] = x0[j] + sizes[j];
    }

    tt = 0.0;
    for (j = 0; j < 3; j++) tt = tt + x0[j] * direc[j];//the projection distance along the incident direction
    for (j = 0; j < 3; j++) R[j] = x0[j] - direc[j] * tt;//the vector of impact parameter
    r2 = 0.0;
    for (j = 0; j < 3; j++) r2 = r2 + R[j] * R[j];//(the impact parameter modulus)**2

    if (r2 != r2) {
        printf("rbs ions collision wrong, see %s line %d \n", __FILE__, __LINE__);
        exit(1);
    }

    proba = 1.0 / (2.0 * PI * debyeMagnit * debyeMagnit) * exp(- r2 / (2.0 * debyeMagnit * debyeMagnit) );//Encounter probability, 2D normal distribution

    while (proba > 1.0) {
        proba = proba - 1.0;
        count++;
    }
    if (proba > rbssimu -> rand()) count++;

    return count;
}

void RBSsimuNS::NRA::emitDirections(const double dtheta, const double dfii, const double sa, double *yr) {

    /***
     * Calculate the emit directions of virtual ions.
     * dtheta: detector polar angle [Radians];
     * dfii: detector azimuthal angle [Radians];
     * sa: spread angle [Radians];
     * yr: backscattered direction (an array of length 3).
     *
     * Results:
     * Randomly choose a direction towards the detector which is inside the cone determined by the
     * spread angle. This direction will be the backscattered direction of virtual ion, yr.
     */

    double l, m, n, fii;
    double ct, st, cf, sf, k;

    l = sin(dtheta) * cos(dfii);
    m = sin(dtheta) * sin(dfii);
    n = cos(dtheta);

    fii = 2.0 * PI * rbssimu -> rand();
    ct = 1.0 - rbssimu -> rand() * (1.0 - cos(sa));//ct = cos(theta);
    st = sqrt(1.0 - ct * ct); //st = sin(theta);
    if(std::isnan(st)) {
        printf("The setting for spread angle has problem! See %s line %d \n", __FILE__, __LINE__);
        exit(1);
    }
    cf = cos(fii);
    sf = sin(fii);
    k = sqrt(1.0 - n * n);

    if (k != 0){
        yr[0] = l * ct + st / k * (l * n * cf - m * sf);
        yr[1] = m * ct + st / k * (m * n * cf + l * sf);
        yr[2] = n * ct - k * st * cf;
    }
    else {
        yr[0] = st * cf;
        yr[1] = st * sf;
        yr[2] = ct;
    }

}

double RBSsimuNS::NRA::elasticEner(const double m1, const double m2, const double cosAng, const double ein) {

    /***
     * Calculate the energy of incident ion after an elastic scattering.
     * m1, m2: the atomic mass of incident ion and target atom;
     * cosAng: cos(scattering angle);
     * ein: the energy of incident ion [eV].
     *
     * Return:
     * eout, the scattering energy [eV].
     */

    double t1, sqfactor;
    double eout;

    t1 = m2 * m2 - m1 * m1 * (1.0 - cosAng * cosAng);
    //Actually, if t1 < 0, it means that the scattering ion is moving to an impossible direction,
    //in which case no virtual ion should be generated.
    //This can happen when m1 > m2 or m1 = m2, and we are asking backscattering.
    if (t1 < 0.0) { printf ("warning: rbs weight calculation failed, see %s line %d\n", __FILE__, __LINE__); t1 = 0.0; }

    if (m1 < m2) sqfactor = (m1 * cosAng + sqrt(t1)) / (m1 + m2);//sqrt(kinematical factor)
    else sqfactor = (m1 * cosAng - sqrt(t1)) / (m1 + m2);

    eout = ein * sqfactor * sqfactor;

    return (eout);
}

double RBSsimuNS::NRA::nuclearEner(const double m1, const double m2, const double m3, const double m4,
                                   const double cosAng, const double ein, const double eq) {
    /***
     * Calculate the energy of emitted nucleus from the nuclear reaction.
     * m1, m2, m3 and m4: the atomic mass of incident nucleus, target nucleus, emitted nucleus and residual nucleus;
     * cosAng: cos(scattering angle);
     * ein: the energy of incident nucleus [eV];
     * eq: the reaction Q energy [eV].
     *
     * Return:
     * eout, the energy of emitted nucleus [eV].
     */

    double E1, Et;
    double M1, M2, M3, M4;
    double sinAng2;
    double A, B, C, D;
    double eout;

    E1 = ein;
    Et = E1+ eq;
    M1 = m1;
    M2 = m2;
    if (m3 < m4) {
        M3 = m3;
        M4 = m4;
    }
    else {
        M3 = m4;
        M4 = m3;
    }
    sinAng2 = 1 - cosAng * cosAng;

    //A = M1 * M4 * (E1 / Et) / ( (M1 + M2) * (M3 + M4) );
    B = M1 * M3 * (E1 / Et) / ( (M1 + M2) * (M3 + M4) );
    //C = M2 * M3 / ( (M1 + M2) * (M3 + M4) ) * (1 + (M1 * eq) / (M2 * Et) );
    D = M2 * M4 / ( (M1 + M2) * (M3 + M4) ) * (1 + (M1 * eq) / (M2 * Et) );

    if (B <= D) eout = Et * B * pow(cosAng + sqrt(D / B - sinAng2), 2);
    else {
        if (0 == world_rank) {
            printf("Error: the reaction may give double values on the energy of emitting particles, which cannot be handled by the program\n");
            printf("Arguments of NRA::nuclearEner(): %.3f %.3f %.3f %.3f %.3f %.3e eV %.3e eV\n", m1, m2, m3, m4,
                   cosAng, ein, eq);
        }
        exit(1);
    }

    return eout;
}

double RBSsimuNS::NRA::getCS(const double E, std::vector<double> &E_list, std::vector<double> &CS_list) {

    /***
     * Calculate the cross section at a specific energy E by using a linear interpolation.
     * E: energy [eV];
     * E_list: a vector of energy [eV];
     * CS_list: a vector of differential cross section [A^2/steradian].
     *
     * Return:
     *      the differential cross section at E.
     */

    int ihalf, n;
    static int iup = 0, idown = 0;
    double cs;

    n = E_list.size();

    /*
      Get two nearest elements with binary search
   */
    if (E > E_list[n-1]) {
        printf("Error: In the calculation of NRA cross section, the energy of the ion is %lf large than the maximum one %lf in input file\n", E, E_list[n-1]);
        exit(1);
    }

    if (E < E_list[0]) {
        printf("Error: In the calculation of NRA cross section, the energy of the ion is %lf smaller than the minimum one %lf in input file\n", E, E_list[0]);
        exit(1);
    }

    if (!(E > E_list[idown] && E < E_list[iup] && (iup - idown) == 1 )){
        iup = n-1; idown = 0;
        while (1) {
            ihalf = idown + (iup - idown) / 2;
            if (ihalf == idown) break;
            if (E_list[ihalf] < E) idown = ihalf;
            else iup = ihalf;
        }
    }

    /* Get the cross section with a simple linear interpolation */
    cs = CS_list[idown] * (E_list[iup] - E) / (E_list[iup] - E_list[idown]);
    cs += CS_list[iup] * (E - E_list[idown]) / (E_list[iup] - E_list[idown]);

    return(cs);
}

int RBSsimuNS::NRA::amorphous_slowdown(const double &z1, const double &mass1, double &xv, double &yv, double &zv, double &lv, double &mv, double &nv,
                                       double &ev, const std::vector<double> &v_loss, const std::vector<double> &e_loss) {

    /***
     * Calculate the amorphous slowdown of backscattered virtual ion.
     * It is similar to RBSsimu::amorphous_slowdown(), but should provide a better interface.
     *
     * z1, mass1: atomic number and mass of the backscattered virtual ion;
     * xv, yv, zv: coordinates of the backscattered virtual ion;
     * lv, mv, nv: represents the moving direction of backscattered virtual ion;
     * ev: energy of the backscattered virtual ion.
     * v_loss & e_loss: list of velocities (m/s) and corresponding electronic stopping power (eV/Angstrom);
     *
     * Modified:
     *          xv, yv, zv, lv, mv, nv, en
     *
     * Return:
     *          flag (0: does not reach the target surface; 1: reach the target surface, and ready to be detected)
     */

    const double                    &rmax = target -> rmax;
    const int                       *pbc = target -> pbc;
    const double                    *prd = target -> prd;
    const double                    *xlo = target -> xlo;

    //const double                    &z1 = incident -> z;
    //const double                    &mass1 = incident -> mass;
    const double                    &estop = incident -> estop;

    const int                       &ntype = atom -> ntype;
    const std::vector<int>          &itypen = atom -> itypenum;//total atom number for each element
    const int                       &natotal = atom -> natotal;
    const std::vector<double>       &amass = atom -> mass;
    const std::vector<double>       &az = atom -> z;
    const std::vector<std::vector<double> > &matrix = atom -> scat_matrix;
    const double                    &meanm = atom -> meanmass;
    const double                    &meanz = atom -> meanz;

    const double                    &depth_bin = parameter -> freedist;
    const int                       locDenflag = parameter -> locDenflag;
    const int                       locDenflag2 = parameter -> locDenflag2;

    const int                       ffp_mode = rbssimu -> ffp_mode;
    const int                       impact_mode = rbssimu -> impact_mode;

    int flag = 0;
    int i;
    int binCount;
    double l, m, n, px, py, pz, en;
    double z2, mass2, Natom, Natom2;
    double p, au, factor, meanau, meanepsi, meanfactor, meanecm;
    double flightlength;
    double t, ran;
    double locDen;

    //printf("Loc: %.3f %.3f %.3f; Ang: %.3f %.3f %.3f; En = %.3f\n", px, py, pz, l, m, n, en);
    //exit(1);

    px = xv;
    py = yv;
    pz = zv;

    l = lv;
    m = mv;
    n = nv;

    en = ev;

    Natom = 1 /(4 * PI * rmax * rmax * rmax/3);

    while (en > estop) {

        if (pz <= xlo[2]){
            flag = 1;
            xv = px;
            yv = py;
            zv = pz;
            lv = l;
            mv = m;
            nv = n;
            ev = en;
            break;
        }

        t = 0.0;
        i = 0;
        ran = rbssimu -> rand();

        //Randomly choose the collision atom type
        while(1){
            t = t + (double) (itypen[i]) / (double) (natotal);
            if (t >= ran) break;
            else i++;
            if (i >= ntype) {
                if (0 == world_rank) printf("Error: in NRA::amorphous_slowdown, see %s %d, probability %lf, random number %lf\n", __FILE__, __LINE__, t, ran);
                exit(1);}
        }

        //Select the mode of free flight path
        if(0 == ffp_mode){
            flightlength = rmax * 10.0 * rbssimu -> logrand();
        }
        else if(1 == ffp_mode){
            flightlength = depth_bin;
        }
        else if(2 == ffp_mode){
            meanau = 0.8853 / (pow(z1, 0.23) + pow(meanz, 0.23)) * 0.52917721092;//mean universal screening length
            meanfactor = meanau / (z1 * meanz * E2);
            meanecm = en / (mass1 / meanm + 1.0);
            meanepsi = meanfactor * meanecm;

            flightlength = (0.02*pow(1+mass1/meanm,2) / (4*PI*meanau*meanau*Natom)) * (meanepsi*meanepsi + 0.052*pow(meanepsi,1.32)) / log(1 + meanepsi);
        }
        else{
            flightlength = rmax * 10.0 * rbssimu -> logrand();
        }

        mass2 = amass[i];
        z2 = az[i];
        au = 0.8853 / (pow(z1, 0.23) + pow(z2, 0.23)) * 0.52917721092;//Universal screening length
        factor = au / (z1 * z2 * E2);

        //Check the local density variation
        if (0 == locDenflag & 0 == locDenflag2) Natom2 = Natom;
        else if (1 == locDenflag && 0 == locDenflag2) {
            //Find the bin number where the current virtual ion is located
            binCount = rbssimu -> den_bin_select(px, py, pz);
            //(Calculate the local value of Natom2 in the future)
            Natom2 = Natom;
            if (0 == world_rank) printf("Error: in NRA::amorphous_slowdown, the deployment of this feature is not finished yet. See %s %d", __FILE__, __LINE__);
            exit(1);
        }
        else if (0 == locDenflag && 1 == locDenflag2){
            //Calculate the local value of Natom2 according to the ion position
            target -> loc2DensityCheck(px, py, pz, locDen, Natom2);
        }
        else if (locDenflag == 1 && locDenflag2 == 1) {
            if (0 == world_rank) printf("The value of locDenflag and locDenflag2 cannot be both 1.\n");
            exit(1);
        }
        else{
            if (0 == world_rank) printf("The value of locDenflag (%d) or locDenflag2 (%d) has problem.\n", locDenflag, locDenflag2);
            exit(1);
        }

        /*
         Descriminate the FFP according to the approximate mean atomic separation Natom**(-1/3);
         Make sure the amorphous slowing down process stops at the target surface
         */
        if (flightlength >= pow(Natom,-1/3)){

            px = px + flightlength * l;
            py = py + flightlength * m;
            pz = pz + flightlength * n;


            if(pz > xlo[2]){

                //Calculate electronic energy loss
                ele_ener_variation_FFP(flightlength, depth_bin, en, mass1, locDenflag, binCount, locDenflag2, locDen, v_loss, e_loss);

                //Calculate the impact parameter
                //If p = -1, no nuclear collision
                if(0 == impact_mode){
                    if (0 == Natom2) p = -1;
                    else p = sqrt(rbssimu -> rand() / (PI * Natom2 * flightlength));
                }
                else if(1 == impact_mode){
                    if (0 == Natom2) p = -1;
                    else p = sqrt(-1 * log(rbssimu -> rand())/(PI * Natom2 * flightlength));
                }
                else if(2 == impact_mode){
                    //This mode should be abandoned
                    p = sqrt(rbssimu -> rand() * rmax * rmax * rmax / (PI * flightlength));
                }
                else{
                    if (0 == Natom2) p = -1;
                    else p = sqrt(rbssimu -> rand() / (PI * Natom2 * flightlength));
                }

                //Calculate nuclear energy loss and update moving direction
                rbssimu -> scatter_angle(en, mass1, mass2, factor, au, p, l, m, n);

            } //End of if(pz > xlo[2])

            else{
                rbssimu -> FFP_cutted_by_surface(flightlength, px, py, pz, l, m, n, xlo[2]);
                ele_ener_variation_FFP(flightlength, depth_bin, en, mass1, locDenflag, binCount, locDenflag2, locDen, v_loss, e_loss);
            }

        } //End of if (flightlength >= pow(Natom,-1/3))

        else{

            flightlength = pow(Natom,-1/3);
            px = px + flightlength * l;
            py = py + flightlength * m;
            pz = pz + flightlength * n;

            if(pz > xlo[2]){
                ele_ener_variation_FFP(flightlength, depth_bin, en, mass1, locDenflag, binCount, locDenflag2, locDen, v_loss, e_loss);

                if (0 == Natom2) p = -1;
                else p = sqrt(rbssimu -> rand()/(PI*pow(Natom,2/3)));

                //Calculate nuclear energy loss and update moving direction
                rbssimu -> scatter_angle(en, mass1, mass2, factor, au, p, l, m, n);

            } //End of if(pz > xlo[2])

            else{
                rbssimu -> FFP_cutted_by_surface(flightlength, px, py, pz, l, m, n, xlo[2]);
                ele_ener_variation_FFP(flightlength, depth_bin, en, mass1, locDenflag, binCount, locDenflag2, locDen, v_loss, e_loss);
            }

        } //End of else (flightlength >= pow(Natom,-1/3))

        if (pz <= xlo[2]){
            flag = 1;
            xv = px;
            yv = py;
            zv = pz;
            lv = l;
            mv = m;
            nv = n;
            ev = en;
            break;
        }

        if (pbc[2] == 0 && pz > prd[2]) return 0;

    }

    return flag;
}

int RBSsimuNS::NRA::detected(const double *yr, const double dtheta, const double dfii, const double ddist,
                             const double dradi) {

    /***
     * Determine if a backscattered virtual ion can be detected after leaving the target surface.
     * yr: represents the moving direction of the virtual ion at the target surface (expressed in spherical coordinates)
     * dtheta, dfii: polar and azimuthal angles of detector [steradian]
     * ddist: The distance between the detector and the target [cm]
     * dradi: The radius of the detector [mm]
     *
     * Return:
     *          flag (0: does not detected; 1: detected)
     */

    int i;
    double ddetec[3], cosemit, cdetec;

    ddetec[0] = sin(dtheta) * cos(dfii);
    ddetec[1] = sin(dtheta) * sin(dfii);
    ddetec[2] = cos(dtheta);
    cosemit = 0.0;

    for (i = 0; i < 3; i++) cosemit = cosemit + yr[i] * ddetec[i];//angle between the detector and the backscattered ion
    cdetec = ddist * 10.0 / sqrt(dradi * dradi + ddist *10.0 * ddist * 10.0);

    if (cosemit >= cdetec) return 1;
    else return 0;
}

void RBSsimuNS::NRA::getSignal(std::vector<double> &yield, const double energy, const double weight, const int nchannel,
                               const double slope, const double intercept, const double sigmach,
                               const std::vector<int> channel) {

    /***
     * Calulate the NRA yield.
     *
     * yield: NRA yield;
     * energy: the energy of the virtual ion being detected [eV];
     * weight: the differential cross section of the backscattering event in which the virtual ion is created [A^2/steradian];
     * nchannel: the number of channel registered in the detector;
     * slope, intercept: the detector's energy slope and intercept [eV/Channel] [eV];
     * sigmach: the standard deviation of detector [channel];
     * channel: the detector channels.
     *
     * Modified:
     *          yield.
     */

    int i;
    double ch;

    ch = (energy / 1000.0 - intercept) / slope;

    for (i = 0; i < nchannel; i++){
        yield[i] = yield[i] + weight * 1.0 / (sigmach * sqrt(2.0 * PI)) * exp(- (channel[i] + 0.5 - ch) * (channel[i] + 0.5 - ch) / (2.0 * sigmach * sigmach));
    }
}

void RBSsimuNS::NRA::printNRA(const int &num, const int &ionNumberPerProc, const int &ionNumberPerProcOdd) {

    /**
     * Print NRA yield.
     * num: the number of incident ion;
     * ionNumberPerProc: the total number of ion in each processor (from Rank-0 to Rank-n)
     * ionNumberPerProcOdd: the total number of ion in processor-1 to processor-n)
     *
     * Only rank-0 prints NRA spectra, which is NRATotalYield.
     * When num < ionNumberPerProcOdd, the NRAyield of all processors are summed by MPI_Reduce(),
     * in addition, at the moment of num+1 == ionNumberPerProcOdd, after the summation,
     * NRAyield of rank-0 will be updated to NRATotalYield;
     * When num >= ionNumberPerProcOdd, NRATotalYield in rank-0 is equal to NRAyield of rank-0.
     */

    int i;
    int nprint = 40;
    const int &nchannel = parameter -> nchannel;
    const std::vector<int> channel = rbssimu -> channel;

    std::ofstream nraout;

    if ((num + 1) % nprint == 0 || (num + 1) == ionNumberPerProc || (num + 1) == ionNumberPerProcOdd) {

        if (num < ionNumberPerProcOdd) {
            MPI_Barrier(MPI_COMM_WORLD);
            MPI_Reduce(NRAyield.data(), NRATotalYield.data(), nchannel, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

            if ( (num + 1) == ionNumberPerProcOdd) {
                if (0 == world_rank) {
                    for (i = 0; i < nchannel; i++) NRAyield[i] = NRATotalYield[i];
                }
            }
        }

        if (0 == world_rank && num >= ionNumberPerProcOdd) {
            for (i = 0; i < nchannel; i++) NRATotalYield[i] = NRAyield[i];
        }

        if (0 == world_rank) {
            nraout.open("out/nraspectra.dat", std::ios::out);
            if (!nraout) {
                printf("Error: cannot open the out/nraspectra.dat file\n");
                nraout.close();
                exit(1);
            }
            else {
                for (i = 0; i < nchannel; i++) {
                    nraout << std::setw(10) << std::right << channel[i] << "  " << std::scientific << NRATotalYield[i] << std::endl;
                }
                nraout.close();
            }
        }
    }


}

void RBSsimuNS::NRA::ele_ener_variation_FFP(double ffp, const double bin, double &en, const double mass1,
                                     const int locflag, const int binCount, const int locflag2, const double locDen,
                                     const std::vector<double> &v_loss, const std::vector<double> &e_loss){
    /**
     Take into account the electronic energy loss variation in the FFP.
     Update the backscattered ion energy.

     It is similar to RBSsimu::ele_ener_variation_FFP(), but use a dedicated electronic energy loss database for emitted nucleus:
     - v_loss & e_loss: list of velocities (m/s) and corresponding electronic stopping power (eV/Angstrom);
     */

    double vv;
    double loss;
    double e = 1.6021892e-19;
    double u = 1.6605655e-27;
    double denRatio = 1.0;
    const std::vector<double> &binDen = target -> binDen;

    if (0 == locflag && 0 == locflag2) denRatio = 1;
    else if (0 == locflag && 1 == locflag2) {
        denRatio = locDen / parameter -> density;
    }
    else if (1 == locflag && 0 == locflag2) {
        if (binCount < 0) denRatio = 1;
        else denRatio = binDen.at(binCount);
    }
    else if (locflag == 1 && locflag2 == 1) {
        printf("The value of locDenflag and locDenflag2 cannot be both 1.\n");
        exit(1);
    }
    else{
        printf("The value of locDenflag (%d) or locDenflag2 (%d) has problem.\n", locflag, locflag2);
        exit(1);
    }

    while(ffp > bin){
        vv = sqrt (2.0 * en * e / (mass1 * u));
        loss = getESP(vv, v_loss, e_loss) * denRatio;
        en = en - loss * bin;
        ffp = ffp - bin;
    }

    vv = sqrt (2.0 * en * e / (mass1 * u));
    loss = getESP(vv, v_loss, e_loss) * denRatio;
    en = en - loss * ffp;
}

const double RBSsimuNS::NRA::getESP(const double v1, const std::vector<double> v_loss, const std::vector<double> e_loss) {
    /**
     * v1: ion velocity [m/s];
     * v_loss & e_loss: list of velocities (m/s) and corresponding electronic stopping power (eV/Angstrom);
     *
     * Return the electronic energy loss [ev/Angstrom] at velocity v1
     */

    int i, ihalf, n;
    static int iup = 0, idown = 0;
    double vreal, Se;
    double scale;

    vreal=v1;
    n=v_loss.size();

    /*
      Get two nearest elements with binary search
   */
    if (vreal > v_loss[n-1]) {
        printf("Error: in NRA::getESP(), the velocity of the ion is %lf large than the maximum one %lf in input file\n", vreal, v_loss[n-1]);
        exit(1);
    }
    if (vreal < v_loss[0]) {
        printf("Error: in NRA::getESP(), the velocity of the ion is %lf smaller than the minimum one %lf in input file\n", vreal, v_loss[0]);
        exit(1);
    }

    if (!(vreal > v_loss[idown] && vreal < v_loss[iup] && (iup - idown) == 1 )){
        iup = n-1; idown = 0;
        while (1) {
            ihalf = idown + (iup - idown) / 2;
            if (ihalf == idown) break;
            if (v_loss[ihalf] < vreal) idown = ihalf;
            else iup = ihalf;
        }
    }

    scale = 1.0;
    /* Get elstop with a simple linear interpolation */
    Se = e_loss[idown] * (v_loss[iup] - vreal) / (v_loss[iup] - v_loss[idown]);
    Se += e_loss[iup] * (vreal - v_loss[idown]) / (v_loss[iup] - v_loss[idown]);
    Se *= scale;

    return(Se);
}