#include <fstream> 
#include <iostream>
#include <sstream> 
#include <cmath>
#include <mpi.h>

#include "atom.h"
#include "atom_type.h"
#include "parameter.h"
#include "target.h"

using namespace RBSsimuNS;

RBSsimuNS::Atom::Atom(RBSsimu *rbs):Pointer(rbs) {
    
}

RBSsimuNS::Atom::~Atom() {
    delete[] x[0];
    delete[] x;
}

void RBSsimuNS::Atom::init() {
    int i, j;
    double tmp;

    ntype = parameter -> ntype;
    symbol = parameter -> atomsymbol;
    vibrationmagn = parameter -> vibration; //Added by JIN

    mass.resize(ntype, 0);
    z.resize(ntype, 0);
    edisplacement.resize(ntype, 0);

    for(i = 0; i < ntype; i++){
        j = atom_type -> value_atom_type(symbol[i], z[i], mass[i], edisplacement[i], tmp);
        if(j == 0) {
            printf("can not find the target atom type %s, check !!\n", symbol[i].c_str());
            exit(1);
        }
    }

    itypenum.resize(ntype, 0);
    debyemagn.resize(ntype, 0);

    scat_matrix.resize(DIME, std::vector<double>(DIMS, 0));
    time_matrix.resize(DIME, std::vector<double>(DIMS, 0));
}

void RBSsimuNS::Atom::read_coordinate() {
    int i, j;
    int count = 0;

    std::string tmp;
    std::string line;
    std::stringstream ss;
    std::ifstream readcoord;

    /*
     Read the first line for the total atom number;
     Skip the second line;
     Read remaining lines for the 3D coordinates and the atom type.

     Object:
     Get the atom 3D coordinates;
     Get the atom type of each atom;
     Count the total number in each atom type;

     For other results, please go to:
     RBSsimuNS::Atom::properties()
     */

    readcoord.open(CoordsPath);
    if(!readcoord){
        std::cerr << "Oh no," << CoordsPath <<  " could not be opened for reading!" << std::endl;
        exit(1);
    }
    
    getline(readcoord, line);
    ss.str(line);
    ss >> nat;
    getline(readcoord, line);

    if(nat > 0){
        itype.resize(nat, 0);
        //x.resize(nat, std::vector<double>(3, 0)); //Keyword: vector_memory
        allocate_2d<double>(x, nat, 3);
    }
    else{
        printf("The read of total atom number (%d) in coords1.in has problem.\n", nat);
        exit(1);
    }

    for(i = 0; i < nat; i++) {
        ss.clear();
        getline(readcoord, line);
        ss.str(line);
        if (ss >> tmp) {
            count++;
            for (j = 0; j < 3; j++) ss >> x[i][j];
            ss >> itype[i];
            itype[i] = itype[i] - 1;
            if (itype[i] >= ntype || itype[i] < 0) {
                printf("The type of atom No.%d is %d, it is wrong\n", i, itype[i] + 1);
                exit(1);
            }
            itypenum[itype[i]]++;
            }
        else {
            printf("Format of coords1.in is wrong!\n");
            exit(1);
        }
    }

    if (count != nat) {
        printf("Error: only found %d atoms in a total of %d atoms\n", count, nat);
        readcoord.close();
        exit(1);
    }

    readcoord.close();

    properties();
}

void RBSsimuNS::Atom::read_scatter_matrix() {
    /*
     Read Scatter.mat
     The first column represents the reduced energy (By default, DIME: 1.9669533e-06 to 2.0643840e+06);
     The second column represents the reduced impact parameter (By default, DIMS: 1.5366822e-08 to 6.3000000e+01)
     The Third column is the s2: square(sin(thetaCM/2));
     */

    int i,j;
    double x, y;

    std::string line;
    std::stringstream ss;
    std::ifstream readScat;

    readScat.open(ScatterPath);
    if(!readScat){
        std::cerr << "Oh no," << ScatterPath <<  " could not be opened for reading!" << std::endl;
        exit(1);
    }

    for (i = 0; i < DIME; i++){
        for(j = 0; j < DIMS; j++){
            ss.clear();
            getline(readScat, line);
            ss.str(line);
            if (ss >> x && ss >> y) ss >> scat_matrix[i][j];
        }
    }

    readScat.close();
}

void RBSsimuNS::Atom::read_time_matrix() {
    /*
     Read Time.mat
     The first column represents the reduced energy (By default, DIME: 1.9669533e-06 to 2.0643840e+06);
     The second column represents the reduced impact parameter (By default, DIMS: 1.5366822e-08 to 6.3000000e+01)
     The Third column is equal to: (time integral)/(universal screening length);
     */

    int i,j;
    double x, y;

    std::string line;
    std::stringstream ss;
    std::ifstream readTim;

    readTim.open(TimePath);
    if(!readTim){
        std::cerr << "Oh no," << ScatterPath <<  " could not be opened for reading!" << std::endl;
        exit(1);
    }

    for (i = 0; i < DIME; i++) {
        for (j = 0; j < DIMS; j++) {
            ss.clear();
            getline(readTim, line);
            ss.str(line);
            if (ss >> x && ss >> y) ss >> time_matrix[i][j];
        }
    }

    readTim.close();
}

void RBSsimuNS::Atom::properties() {
    /*
     Serve for RBSsimuNS::Atom::read_coordinate()
     and calculate the radius of the average atomic density
     and the atomic thermal vibration magnitude
     and the average atomic number and average atomic mass number

     New things (AbJ)
     Vibration mode:
     if 0, computed based an analytical solution of Debye theory (original method);
     if 1, read from user input.
     if 2, use the Debye theory, but change the constant to 12.063 which is used in
     the literature (K. Nordlund, NIMB, (435), 2018)
     */

    int i;
    int tnum = 0;
    double tmass = 0.0;
    double tznum = 0.0;
    double amass;

    double density = parameter->density;
    double temperature = parameter->temperature;
    double dtem = parameter->debyetemperature;
    double *rmax = &(target->rmax);

    for (i = 0; i < ntype; i++) {
        tmass = tmass + itypenum[i] * mass[i];
        tznum = tznum + itypenum[i] * z[i];
        tnum = tnum + itypenum[i];
    }

    natotal = tnum;
    amass = tmass / tnum;
    meanmass = amass;
    meanz = tznum / tnum;
    ndensity = density / amass * 6.02 / 10.0; //Unit: n/A^3

    *rmax = pow(3.0 / (ndensity * 4.0 * PI), 1.0 / 3.0);

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    for (i = 0; i < ntype; i++){
        if(0 == vibrat_mode){
            debyemagn[i] = 21.3 * sqrt(temperature / mass[i]) / (sqrt(3.0) * dtem);
            if (0 == world_rank) printf("Debyemagn[%d]: %.3f\n", i, debyemagn[i]);
        }
        else if(1 == vibrat_mode) {
            debyemagn[i] = vibrationmagn[i];
        }
        else if(2 == vibrat_mode) {
            debyemagn[i] = 12.063 * sqrt(temperature / mass[i]) / dtem;
            if (0 == world_rank) printf("Debyemagn[%d]: %.3f\n", i, debyemagn[i]);
        }
        else{
            debyemagn[i] = 21.3 * sqrt(temperature / mass[i]) / (sqrt(3.0) * dtem);
            printf("The atomic thermal vibration has no mode %d, mode 0 will be applied.\n",vibrat_mode);
        }
    }
}
