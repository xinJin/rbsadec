//
// Created by jinxinjx on 29/03/2022.
//

#ifndef REBUILD_NRASIMU_H
#define REBUILD_NRASIMU_H

#include <vector>
#include <string>

#include "pointer.h"

namespace RBSsimuNS {

    class NRA : public Pointer {
    public:

        NRA(RBSsimu *rbs);
        ~NRA();

        int                         reactionType;
        double                      spreadAng;
        double                      weight;

        std::string                 nucleusSymbol;
        std::string                 emitNucSymbol;
        std::string                 residualNucSymbol;
        std::string                 crossSecPath;
        std::string                 emitStopPowerPath;

        std::vector<double>         emitNucZ;
        std::vector<double>         emitNucMass;
        std::vector<double>         residualNucZ;
        std::vector<double>         residualNucMass;
        double                      Qenergy;
        std::vector<double>         EnerCS;
        std::vector<double>         diffCS;
        std::vector<double>         v_loss;
        std::vector<double>         e_loss;
        std::vector<double>         NRAyield;
        std::vector<double>         NRATotalYield;

        void init();
        void readCS(const std::string inputPath, std::vector<double> &ein, std::vector<double> &cs);
        void read_ele_energyloss(const std::string stopPath, std::vector<double> &v_loss, std::vector<double> &e_loss);
        void emit();
        int virtualIon(const double *xin, const double *xatom, const double *direc, const int *pbc, const double *sizes, const double debyeMagnit);
        void emitDirections(const double dtheta, const double dfii, const double sa, double *yr);
        double elasticEner(const double m1, const double m2, const double cosAng, const double ein);
        double nuclearEner(const double m1, const double m2, const double m3, const double m4, const double cosAng, const double ein, const double eq);
        double getCS(const double E, std::vector<double> &E_list, std::vector<double> &CS_list);
        int amorphous_slowdown(const double &z1, const double &mass1, double &xv, double &yv, double &zv, double &lv, double &mv, double &nv,
                               double &ev, const std::vector<double> &v_loss, const std::vector<double> &e_loss);
        int detected(const double *yr, const double dtheta, const double dfii, const double ddist, const double dradi);
        void getSignal(std::vector<double> &yield, const double energy, const double weight, const int nchannel, const double slope, const double intercept,
                       const double sigmach, const std::vector<int> channel);
        void printNRA(const int &num, const int &ionNumberPerProc, const int &ionNumberPerProcOdd);
        void ele_ener_variation_FFP(double ffp, const double bin, double &en, const double mass1, const int locflag, const int binCount,
                                    const int locflag2, const double locDen, const std::vector<double> &v_loss, const std::vector<double> &e_loss);
        const double getESP(const double v1, const std::vector<double> v_loss, const std::vector<double> e_loss);
    };

}


#endif //REBUILD_NRASIMU_H
