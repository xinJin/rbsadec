#include <math.h>      
#include <fstream>
#include <iomanip>

#include "target.h"
#include "parameter.h"
#include "atom.h"
#include "incident.h"

using namespace RBSsimuNS;

RBSsimuNS::Target::Target(RBSsimu *rbs):Pointer(rbs) {
    minimum_binsize = 1e10;
}

RBSsimuNS::Target::~Target() {
    
}

void RBSsimuNS::Target::init() {
    /*
     Initialization for the class Target (manually).
     */

    int i;
    double size = parameter -> depthsize;

    nuclossflag = 0;
    nucdepthnum = 50;

    recoilspectraflag = 0;
    recspecsize = 100;
    maxrecsize = 0;
    drecspec = 5.0;

    lastcollnum = 0;

    binsize_den.resize(3, 0);
    binnum_den.resize(3, 1);

    for(i = 0; i < 3; i++){
        pbc[i] = parameter -> boundary[i];//boundary condition
        prd[i] = parameter -> boxsize[i];//box size
        xlo[i] = parameter -> xlo[i];//Minimum boundary
        xhi[i] = parameter -> xlo[i] + parameter -> boxsize[i];//Maximum boundary
        binsize[i] = parameter -> binsize[i];//bin size
        binsize_den[i] = parameter -> binsize[i];//bin size (used in local density calculation)
    }

    checkindex.resize(MAXCHECKNUM, 0);
    debyedisp.resize(MAXCHECKNUM, std::vector<double>(3, 0));
    depth.resize(nucdepthnum, 0);
    nucloss.resize(nucdepthnum, 0);
    ndisp.resize(nucdepthnum, 0);
    rece.resize(recspecsize, 0);
    irecspec.resize(recspecsize, 0);
    recspec.resize(recspecsize, 0);

    collindex.resize(MAXCOLLNUM, 0);
    coll_dist.resize(MAXCOLLNUM, 0);
    coll_para.resize(MAXCOLLNUM, 0);
    coll_disp.resize(MAXCOLLNUM, std::vector<double>(3, 0));
    lastcollindex.resize(MAXCOLLNUM, 0);
    angle.resize(MAXCOLLNUM, 0);
    nuclear_energy_loss.resize(MAXCOLLNUM, 0);

    depthstep = size / nucdepthnum;
    if (depthstep == 0.0) {printf ("Depthstep can not be zero, see %s %d", __FILE__, __LINE__); exit(1);}
    for (i = 0; i < nucdepthnum; i++) { depth[i] = (i + 0.5) * depthstep; nucloss[i] = 0; ndisp[i] = 0.0;}
    for (i = 0; i < recspecsize; i++) { rece[i] = exp( (i * 1.0) / drecspec); irecspec[i] = 0; recspec[i] = 0.0;}

}

void RBSsimuNS::Target::bin() {
    /*
     Calculate the number of bin in each direction;
     Re-calculate the bin-size according to the number of bin;
     Apply the periodic boundary conditions on the target.

     Use head[] and list[] to group (and to link) atoms belonging to certain bin;
     head[] stores the last atom in the bin;
     list[] stores the remaining atoms in the bin (the first element in list[] is junk).

     e.g.:
     atoms: 0, 1, 2, 3 in the bin No.0;
     head[0] = 3;
     list[3] = 2;
     list[2] = 1;
     list[1] = 0;
     list[0] = -1 (Used to terminate the iteration over list).

     New things (AbJ)
     locDenflag: A flag for taking into account the local density variation which will
                 have effect on the calculation of electronic energy loss.
     If locDenflag = 0:
                  original method;
     If locDenflag = 1:
                  1) calculate the total mass in each bin;
                  2) calcualte the total number of atoms in each bin;
                  3) calculate the local density (stored in target->binDen).
     */

    int i;
    int numbin;
    int bi[3];
    int ix, iy, iz, ibin;
    int   nat = atom -> nat;//total target atom number
    //std::vector<std::vector<double> > &x = atom -> x;//target atom location //Keyword: vector_memory
    double **x = atom -> x;
    double bindenVolume; //Abj
    double coef; //Abj
    int locDenflag = parameter -> locDenflag;

    for (i = 0; i < 3; i++) bi[i] = int (prd[i] / binsize[i] + COMPUERRO);
    nbinx = bi[0]; nbiny = bi[1]; nbinz = bi[2];

    for (i = 0; i < 3; i++) {
        binsize[i] = prd[i] / bi[i];
        if (binsize[i] < minimum_binsize) minimum_binsize = binsize[i];
    }

    numbin = nbinx * nbiny * nbinz;

    head.resize(numbin, -1);
    list.resize(nat, 0);

    if (0 == locDenflag) {
        for(i = 0; i < nat; i++){
            ix = (floor) ((x[i][0] - xlo[0]) / binsize[0]);
            if (ix < 0 && pbc[0] != 0) ix = (ix % nbinx) + nbinx;
            if (ix >= nbinx && pbc[0] != 0) ix = ix % nbinx;
            if (pbc[0] == 0 && ix < 0) { printf ("warning : %dth atom out of box x, relocated in the first bin.\n", i); ix = 0; }
            if (pbc[0] == 0 && ix >= nbinx) { printf ("warning : %dth atom out of box x, relocated in the last bin.\n", i); ix = nbinx - 1; }

            iy = (floor) ((x[i][1] - xlo[1]) / binsize[1]);
            if (iy < 0 && pbc[1] != 0) iy = (iy % nbiny) + nbiny;
            if (iy >= nbiny && pbc[1] != 0) iy = iy % nbiny;
            if (pbc[1] == 0 && iy < 0) { printf ("warning : %dth atom out of box y, relocated in the first bin.\n", i); iy = 0; }
            if (pbc[1] == 0 && iy >= nbiny) { printf ("warning : %dth atom out of box y, relocated in the last bin.\n", i); iy = nbiny - 1; }

            iz = (floor) ((x[i][2] - xlo[2]) / binsize[2]);
            if (iz < 0 && pbc[2] != 0) iz = (iz % nbinz) + nbinz;
            if (iz >= nbinz && pbc[2] !=0 ) iz = iz % nbinz;
            if (pbc[2] == 0 && iz < 0) { printf ("warning : %dth atom out of box z, relocated in the first bin.\n", i); iz = 0; }
            if (pbc[2] == 0 && iz >= nbinz) { printf ("warning : %dth atom out of box z, relocated in the last bin.\n", i); iz = nbinz - 1; }

            ibin = ix * (nbiny * nbinz) + iy * (nbinz) + iz;
            list[i] = head[ibin];
            head[ibin] = i;
        }
    }

    else if (1 == locDenflag) {
        //std::vector<int> binDen_count(numbin, 0);
        double density = parameter->density;
        double mass = 0;
        double u = 1.6605655e-27;//1u=1.660539040(20)×10−27 kg, unified atomic mass unit

        std::vector<double> dbinsize = parameter -> dbinsize; //Change the bin size dedicated for the local density calculation
        int numbin_den;
        int ibin_den;
        std::vector<int> i_den(3, 0);

        int histnum = 300;
        std::vector<int> histogram(histnum, 0); //Create a histogram for the distribution of local bin density
        int histloc;

        for (i = 0; i < 3; i++) {
            binsize_den.at(i) = dbinsize.at(i);
            binnum_den.at(i) = int (prd[i] / binsize_den.at(i) + COMPUERRO);
            binsize_den.at(i) = prd[i] / binnum_den.at(i);
        }

        numbin_den = binnum_den.at(0) * binnum_den.at(1) * binnum_den.at(2);
        binDen.resize(numbin_den, 0);

        for(i = 0; i < nat; i++){
            ix = (floor) ((x[i][0] - xlo[0]) / binsize[0]);
            if (ix < 0 && pbc[0] != 0) ix = (ix % nbinx) + nbinx;
            if (ix >= nbinx && pbc[0] != 0) ix = ix % nbinx;
            if (pbc[0] == 0 && ix < 0) { printf ("warning : %dth atom out of box x, relocated in the first bin.\n", i); ix = 0; }
            if (pbc[0] == 0 && ix >= nbinx) { printf ("warning : %dth atom out of box x, relocated in the last bin.\n", i); ix = nbinx - 1; }

            iy = (floor) ((x[i][1] - xlo[1]) / binsize[1]);
            if (iy < 0 && pbc[1] != 0) iy = (iy % nbiny) + nbiny;
            if (iy >= nbiny && pbc[1] != 0) iy = iy % nbiny;
            if (pbc[1] == 0 && iy < 0) { printf ("warning : %dth atom out of box y, relocated in the first bin.\n", i); iy = 0; }
            if (pbc[1] == 0 && iy >= nbiny) { printf ("warning : %dth atom out of box y, relocated in the last bin.\n", i); iy = nbiny - 1; }

            iz = (floor) ((x[i][2] - xlo[2]) / binsize[2]);
            if (iz < 0 && pbc[2] != 0) iz = (iz % nbinz) + nbinz;
            if (iz >= nbinz && pbc[2] !=0 ) iz = iz % nbinz;
            if (pbc[2] == 0 && iz < 0) { printf ("warning : %dth atom out of box z, relocated in the first bin.\n", i); iz = 0; }
            if (pbc[2] == 0 && iz >= nbinz) { printf ("warning : %dth atom out of box z, relocated in the last bin.\n", i); iz = nbinz - 1; }

            ibin = ix * (nbiny * nbinz) + iy * (nbinz) + iz;
            list[i] = head[ibin];
            head[ibin] = i;

            for(int j = 0; j < 3; j++){
                i_den.at(j) = (floor) ((x[i][j] - xlo[j]) / binsize_den.at(j) );
                if (i_den.at(j) < 0 && pbc[j] != 0) i_den.at(j) = (i_den.at(j) % binnum_den.at(j)) + binnum_den.at(j);
                if (i_den.at(j) >= binnum_den.at(j) && pbc[j] != 0) i_den.at(j) = i_den.at(j) % binnum_den.at(j);
                if (pbc[j] == 0 && i_den.at(j) < 0) i_den.at(j) = 0;
                if (pbc[j] == 0 && i_den.at(j) >= binnum_den.at(j))  i_den.at(j) = binnum_den.at(j) - 1;
            }

            ibin_den = i_den.at(0) * (binnum_den.at(1) * binnum_den.at(2)) +
                       i_den.at(1) * binnum_den.at(2) +  i_den.at(2);
            mass = atom -> mass [atom -> itype [i] ];
            binDen[ibin_den] += mass; //Total mass in a bin (atomic mass unit)
        }

        bindenVolume = binsize_den.at(0) * binsize_den.at(1) * binsize_den.at(2); //[A^3]
        coef = ( u * 1e3 ) / ( bindenVolume * (1e-24) );

        for(i = 0; i < numbin_den; i++){
            if(binDen.at(i) >= 0){
                binDen.at(i) = binDen.at(i) * coef; //switch unit to [g/cm^3]
                binDen.at(i) = binDen.at(i) / density; //become local density ratio

                //if(binDen.at(i) > 1) printf("\n binDen[%d] = %.5f Larger\n", i, binDen.at(i));
                //else if(binDen.at(i) == 1) printf("\n binDen[%d] = %.5f Equal\n", i, binDen.at(i));
                //else printf("\n binDen[%d] = %.5f Smaller\n", i, binDen.at(i));

                //Create the histogram of loc. den. distribution
                histloc = int (binDen.at(i) * 100 + 0.5);
                if(histloc >= histogram.size()) histogram.at(histogram.size() - 1) += 1;
                else histogram.at(histloc) += 1;
            }
            else if (binDen.at(i) < 0 || isnan(binDen.at(i) ) ){
                printf ("warning : The local density of %dth bin (den) is not normal. \n", i);
                exit(1);
            }
        }

        double boxmass, boxvolume, boxdensity;
        boxmass = atom -> meanmass * atom -> natotal;
        boxvolume = prd[0] * prd[1] * prd[2];
        boxdensity = boxmass * ( u * 1e3 ) / ( boxvolume * (1e-24) );

        printf("\nInput density = %.3f g/cm^3\n", density);
        printf("Box density = %.3f g/cm^3\n", boxdensity);

        //Print the local density distribution.
        std::ofstream denDistribution;
        double di;
        denDistribution.open("out/density_distribution.dat", std::ios::out);
        if (!denDistribution){
            printf("Cannot open the out/density_distribution.dat file\n");
            denDistribution.close();
        }
        else{
            denDistribution << binsize_den[0] << " " << binsize_den[1] << " " << binsize_den[2] << "\n\n";
            for (i = 0; i < histogram.size(); i++){
                di = float (i) / 100;
                if (0 == histogram[i]) continue;
                denDistribution << std::setw(4) << std::right << std::fixed << di
                << "  " << std::setw(10) << std::right << histogram[i] << " "
                << std::setw(15) << std::right << std::scientific <<
                ( float (histogram[i]) / numbin_den) << "\n";
            }
            denDistribution.close();
        }

        printf("The write of local density distribution is finished!\n\n");

    }

    else{
        printf("The value of locDenflag cannot be %d (must be 0 or 1).\n", locDenflag);
        exit(1);
    }
}

void RBSsimuNS::Target::find_check_atom() {
    /*
     Check if the projectile enters into the target;
     Find target atoms in surrounding 3 bins along each direction,
     which depends on if the projectile enters into the target.
     */

    int i,j,k,l,m;
    std::vector<double> xin(incident -> x, incident -> x + 3);
    int into = incident -> into;
    std::vector<int> idbin(3, 0);
    int ix, iy, iz;
    static std::vector<int> o_idbin{0, 0, 0};

    /*
     Modified by J.
     Original:
    for (i = 0; i < 3; i++) idbin[i] = floor (xin[i] / binsize[i]);
     */
    for (i = 0; i < 3; i++) idbin[i] = floor ((xin[i] - xlo[i]) / binsize[i]);

    if(0 == into){
        m = 0;
        for (i = 0; i < 3; i++) o_idbin[i] = 0;
        if (idbin[2] >= 0) incident -> into = 1;
        for (i = -1; i <= 1; i++){
            ix = idbin[0] + i;
            if (ix < 0 && pbc[0] != 0) { ix = ix % nbinx; ix = ix + nbinx;}
            if (ix >= nbinx && pbc[0] != 0) ix = ix % nbinx;
            if ((ix < 0 || ix >= nbinx) && pbc[0] == 0) continue;
            for (j = -1; j <= 1; j++){
                iy = idbin[1] + j;
                if (iy < 0 && pbc[1] != 0){ iy = iy % nbiny; iy = iy + nbiny;}
                if (iy >= nbiny && pbc[1] != 0) iy = iy % nbiny;
                if ((iy < 0 || iy >= nbiny) && pbc[1] == 0) continue;
                for (k = -1; k <= 1; k++){
                    iz = idbin[2] + k;
                    if (iz < 0 ) continue; //Potential problem: if iz >= nbinz, a warining or error should be given.

                    //AdJ, check the bound
                    if(ix * (nbiny * nbinz) + iy * nbinz + iz < 0 || ix * (nbiny * nbinz) + iy * nbinz + iz >= nbinx * nbiny * nbinz){
                        printf("Warning, bound limit overflow, UB! see %s line %d \n", __FILE__, __LINE__);
                        exit(1);
                    }
                    l = head[ix * (nbiny * nbinz) + iy * nbinz + iz];
                    while (l >= 0){
                        checkindex[m] = l;
                        m++;
                        if(m >= MAXCHECKNUM) {printf("Please increase the MAXCHECKNUM listed in target.h \n"); exit(1); }
                        l = list[l];
                    }
                }
            }
        }
        checknum = m;
    }

    else{
        /*
        Modified by J.
        The original if - else clause is canceled.
        Because the purpose of the statement is not clear.
        if (idbin[0] != o_idbin[0] || idbin[1] != o_idbin[1] || idbin[2] != o_idbin[2]){
        //Body text
         }
        else return;
         */

        m = 0;
        /*
        Modified by J.
        Original:
        for (i = 0; i < 3; i++) idbin[i] = floor (xin[i] / binsize[i]);
        */
        for (i = 0; i < 3; i++) idbin[i] = floor ((xin[i] - xlo[i]) / binsize[i]);

        for (i = -1; i <= 1; i++){
            ix = idbin[0] + i;
            if (ix < 0 && pbc[0] != 0) { ix = ix + nbinx;}//Note the difference (potential problem) with that in the previous loop.
            if (ix >= nbinx && pbc[0] != 0) ix = ix - nbinx;
            if ((ix < 0 || ix >= nbinx) && pbc[0] == 0) continue;
            for (j = -1; j <= 1; j++){
                iy = idbin[1] + j;
                if (iy < 0 && pbc[1] != 0){ iy = iy + nbiny;}
                if (iy >= nbiny && pbc[1] != 0) iy = iy - nbiny;
                if ((iy < 0 || iy >= nbiny) && pbc[1] == 0) continue;
                for (k = -1; k <= 1; k++){
                    iz = idbin[2] + k;
                    if (iz < 0 && pbc[2] != 0) { iz = iz + nbinz; }
                    if (iz >= nbinz && pbc[2] != 0)  iz = iz - nbinz;
                    if ((iz < 0 || iz >= nbinz) && pbc[2] == 0) continue;

                    //AdJ, check the bound
                    if(ix * (nbiny * nbinz) + iy * nbinz + iz < 0 || ix * (nbiny * nbinz) + iy * nbinz + iz >= nbinx * nbiny * nbinz){
                        printf("Warning, bound limit overflow, UB! see %s line %d \n", __FILE__, __LINE__);
                        printf("Bin size (x, y, z): %.4f %.4f %.4f\n", binsize[0], binsize[1], binsize[2]);
                        printf("Bin Number (x, y, z): %.d %.d %.d\n", nbinx, nbiny, nbinz);
                        printf("Ion position (x, y, z): %.4f %.4f %.4f\n", xin[0], xin[1], xin[2]);
                        //printf("Ion energy: %.4f\n", energy);
                        printf("Bin position (x, y, z): %.d %.d %.d\n", idbin[0], idbin[1], idbin[2]);
                        printf("Current bin number: %d\n", ix * (nbiny * nbinz) + iy * nbinz + iz);
                        exit(1);
                    }
                    l = head[ix * (nbiny * nbinz) + iy * nbinz + iz];
                    while (l >= 0){
                        checkindex[m] = l;
                        m++;
                        if(m >= MAXCHECKNUM) {printf("Please increase the MAXCHECKNUM listed in target.h \n"); exit(1); }
                        l = list[l];
                    }
                }
            }
        }
        checknum = m;
    }
}

void RBSsimuNS::Target::find_coll_partner() {
    /*
     Search collision partners
     Obtain the atomic position in the interesting region (considering the thermal vibration);
     Calculate the projection length and the impact parameter;
     Calculate the minimum projection length;
     Choose the collision partners according to the method used in MARLOW.

     Discriminate the last collided atoms;
     atoms with negative projection lengths;
     atoms with too large impact parameters.
     */

    int  i, j, m, find;
    //const std::vector<std::vector<double> > &xat = atom -> x; //Keyword: vector_memory
    double **xat = atom -> x;

    const std::vector<int> &itype = atom -> itype;
    const std::vector<double> xin(incident -> x, incident -> x + 3);
    const std::vector<double> y(incident -> y, incident -> y + 3);
    const int debyeflag = parameter -> debyeflag;

    std::vector<double> stt(checknum,1.0e8), sr(checknum, 1.0e8);
    double x0[3], tt, deltat, R[3], r, tmin = 1e10;
    const std::vector<double> &debyemagn = atom -> debyemagn;

    const std::vector<double> &rmax_elem = incident->maximpact; //AbJ
    const double &position_offset=parameter->coffset; //AbJ
    double impact1st; //AbJ, the impact parameter of the atom with the minimum projection distance

    for (i = 0; i < checknum; i++){
        find = 0;
        if (debyeflag){
            for (j = 0; j < 3; j++) debyedisp[i][j] = debyemagn[itype[checkindex[i]]] * rbssimu -> gaussianrand();
        }
        else{
            for (j = 0; j < 3; j++) debyedisp[i][j] = 0.0;
        }

        for (j = 0; j < lastcollnum; j++) { if (checkindex[i] == lastcollindex[j]) find = 1; }
        if (find == 1) continue;

        for (j = 0; j < 3; j++)  x0[j] = xat[checkindex[i]][j] + debyedisp[i][j] - xin[j];//Distance between the checking atom and the incident.
        //When the periodic boundary condition is applied,
        //and the distance is larger than half box size,
        //then do this.
        //Do not understand this part...
        for (j = 0; j < 3; j++){
            if (pbc[j] && x0[j] >   prd[j] / 2.0) x0[j] = x0[j] - prd[j];
            if (pbc[j] && x0[j] < - prd[j] / 2.0) x0[j] = x0[j] + prd[j];
        }

        tt = 0.0;
        for (j = 0; j < 3; j++) tt = tt + x0[j] * y[j];//the projection distance along the incident direction
        if (tt < 0.0) continue;
        for (j = 0; j < 3; j++) {
            R[j] = x0[j] - y[j] * tt;//create the vector of impact parameter
        }
        r = 0.0;
        for (j = 0; j < 3; j++) r = r + R[j] * R[j];
        r = sqrt (r);//calculate the modulus of the impact parameter
        if (r != r) {
            printf("parameter low than zero, see %s line %d \n", __FILE__, __LINE__);
            exit(1);
        }

        if (r > rmax_elem[itype[checkindex[i]]]) continue;//Modified by JIN

        stt[i] = tt; sr[i] = r;//stt: projection length, sr: impact parameter,
        if (tmin > tt) {
            tmin = tt; //find the minimum projection length
            impact1st = sr[i]; //and the associated impact parameter
        }
    }

    m = 0;

    for (i = 0; i < checknum; i++){
        deltat = stt[i] - tmin;
        if (deltat > position_offset || sqrt(deltat * deltat + sr[i] * sr[i]) > rmax_elem[itype[checkindex[i]]]\
            || sqrt(deltat * deltat + impact1st * impact1st) > rmax_elem[itype[checkindex[i]]]) continue; // Modified by JIN, according to M. Robinson, I. Torrens, Phys. Rev. B 9 (1974)
        collindex[m] = checkindex[i];
        coll_para[m] = sr[i];//Collide atom impact parameter
        coll_dist[m] = stt[i];//projection distance along the incident direction
        for (j = 0; j < 3; j++) coll_disp[m][j] = debyedisp[i][j];

        m++;
        if (m >= MAXCOLLNUM){printf("too many collsion partners, see %s line %d \n", __FILE__, __LINE__); exit(1); }
    }

    collnum = m;
    if (collnum == 0) {
        if (tmin < 1e10){ printf("something wrong, see %s line %d \n", __FILE__, __LINE__); exit(1);}
    }
}

void RBSsimuNS::Target::collision() {
    /*
     If no collision partner,
     move to the next bin;
     If have collision partners,
     calculate the scattered angle, energy loss and the time integral.

     Based on the simultaneous collision approximation,
     Update the position of the projectil;
     Update the moving direction of the projectile;

     New things (AbJ)
     locDenflag: A flag for taking into account the local atomic density variation which will
                 have effect on the calculation of electronic energy loss.
     If locDenflag = 0:
                  original method;
     If locDenflag = 1:
                  1) check the number of bin where the ion is currently located;
                  2) calculate the ratio of local atomic density to the global atomic density;
                  3) multiply the electronic energy loss with this local density ratio.
     */

    int i, j;
    const double &z1 = incident -> z;
    const double &estop = incident -> estop;
    const double *xin = incident -> x;
    double *y = incident -> y;
    double &energy = incident -> energy;
    double &range = incident -> prange;
    const double &mass1 = incident -> mass;
    int &moving = incident -> moving;
    const std::vector<int> binLength {nbinx, nbiny, nbinz}; //Abj
    int binExceed = 0; //Abj

    //const std::vector<std::vector<double> > &xat = atom -> x; //Keyword: vector_memory
    double **xat = atom -> x;
    const std::vector<double> &amass = atom -> mass;
    const std::vector<double> &az = atom -> z;// atom z number
    const std::vector<std::vector<double> > &smatrix = atom -> scat_matrix;
    const std::vector<std::vector<double> > &tmatrix = atom -> time_matrix;
    const std::vector<int> &itype = atom -> itype;

    int impactindex, epsiindex;
    double ecm, elab;
    double mass2, z2;
    double factor, au, epsilon, rho;
    double s2, co, time, tancm_half_angle;
    double ct, st, tana;
    int binCount; //Abj
    std::vector<int> idbin(3, 0); //Abj
    double locDen, locADen; //Abj
    int locDenflag = parameter -> locDenflag;
    int locDenflag2 = parameter -> locDenflag2;

    double ytotal[3];
    std::vector<std::vector<double> > yimpac(collnum, std::vector<double>(3, 0.0));
    double x0[3], xr[3], yy;
    std::vector<double> xf(collnum, 0.0);
    double xtotal, fl;

    double v;
    double e = 1.6021892e-19;//1e=1.6021766208(98)×10−19 C, elementary charge
    double u = 1.6605655e-27;//1u=1.660539040(20)×10−27 kg, unified atomic mass unit

    const std::vector<double> &rmax_elem = incident->maximpact; //AbJ

    if (collnum == 0){
        fl = minimum_binsize;
        range = range + fl;
        incident -> move(fl);

        lastcollnum = 0;
        return;
    }

    v = sqrt (2.0 * (energy) * e / (mass1 * u));//energy in eV, mass in atomic mass, velocity in m/s

    for (i = 0; i < collnum; i++){

        mass2 = amass[itype[collindex[i]]];
        z2 = az[itype[collindex[i]]];

        if(locDenflag == 0 && locDenflag2 == 0) {
            elab = (energy) - incident -> eloss(v) * coll_dist[i];//energy left after electronic energy loss before collision
        }
        else if(locDenflag == 0 && locDenflag2 == 1) {
            loc2DensityCheck(xin[0], xin[1], xin[2], locDen, locADen);
            elab = (energy) - incident -> eloss(v) * coll_dist[i] * (locDen / parameter -> density);
        }
        else if(locDenflag == 1 && locDenflag2 == 0) {

            for(int j = 0; j < 3; j++){
                idbin.at(j) = (floor) ((xin[j] - xlo[j]) / binsize_den.at(j) );
                if (idbin.at(j) < 0 && pbc[j] != 0) idbin.at(j) = (idbin.at(j) % binnum_den.at(j)) + binnum_den.at(j);
                if (idbin.at(j) >= binnum_den.at(j) && pbc[j] != 0) idbin.at(j) = idbin.at(j) % binnum_den.at(j);
                if (pbc[j] == 0 && idbin.at(j) < 0) binExceed = 1;
                if (pbc[j] == 0 && idbin.at(j) >= binnum_den.at(j))  binExceed = 1;
            }

            if (1 == binExceed) {
                /*
                 If the pbc is 0, it is possible that the bin number would exceed the limit.
                 In this case, the original method is used, instead of calculating the bin number.
                 Potential problem: at this case, it is better to have no electronic energy loss.
                 */
                elab = (energy) - incident -> eloss(v) * coll_dist[i];
            }
            else {
                binCount = idbin.at(0) * (binnum_den.at(1) * binnum_den.at(2) )
                        + idbin.at(1) * binnum_den.at(2) + idbin.at(2);

                if(binCount < 0 || binCount >= binnum_den[0] * binnum_den[1] *binnum_den[2]){
                    //Bound limit overflow happened here!
                    printf("Warning, bound limit overflow, UB! see %s line %d \n", __FILE__, __LINE__);
                    printf("Bin_DEN size (x, y, z): %.4f %.4f %.4f\n", binsize_den[0], binsize_den[1], binsize_den[2]);
                    printf("Bin_DEN Number (x, y, z): %.d %.d %.d\n", binnum_den[0], binnum_den[1], binnum_den[2]);
                    printf("Ion position (x, y, z): %.4f %.4f %.4f\n", xin[0], xin[1], xin[2]);
                    printf("Ion energy: %.4f\n", energy);
                    printf("Bin_DEN position (x, y, z): %.d %.d %.d\n", idbin[0], idbin[1], idbin[2]);
                    printf("Current bin number: %d\n", binCount);
                    exit(1);
                }

                elab = (energy) - (incident -> eloss(v) * binDen.at(binCount) ) * coll_dist[i];
            }
        }
        else if (locDenflag == 1 && locDenflag2 == 1) {
            printf("The value of locDenflag and locDenflag2 cannot be both 1.\n");
            exit(1);
        }
        else{
            printf("The value of locDenflag (%d) or locDenflag2 (%d) has problem.\n", locDenflag, locDenflag2);
            exit(1);
        }

        ecm = elab / ( mass1 / mass2 + 1.0);//energy in center of mass
        //au is the universal screening length
        //0.8853=0.5*(3*pi/4)**(3/2),0.529 angstrom=Bohr radius
        au = 0.8853 / (pow(z1, 0.23) + pow(z2, 0.23)) * 0.52917721092;
        factor = au / (z1 * z2 * E2);
        //Reduced energy
        //the screening length over the collision diameter (closest approach)
        epsilon = factor * ecm;
        //Reduced distance (x=r/au)
        rho = coll_para[i] / au;

        if (coll_para[i] > rmax_elem[itype[collindex[i]]]){ printf("collision parameter larger than rmax (impossible), see %s line %d", __FILE__, __LINE__); exit(1); }

        //return the row number of the reduced distance used in the s2 and time matrix
        //return the row number of the reduced energy used in the s2 and time matrix
        impactindex = sindex(d2f(rho));
        epsiindex = eindex(d2f(epsilon));
        if (epsiindex > DIME) {printf ("energy too large? see %s line %d", __FILE__, __LINE__);}

        //square(sin(thetaCM/2))
        //Matrix generated from Scatter.mat
        s2 = smatrix[epsiindex][impactindex];
        //time*universal screening length equals to the time integral
        //Matrix generated from Time.mat
        time = tmatrix[epsiindex][impactindex];

        co = sqrt(1.0 - s2);//cos(thetaCM/2)
        if (co == 0.0) tancm_half_angle = 1e10;
        else tancm_half_angle = sqrt(s2) / co;
        ct = 2.0 * (1.0 - s2) - 1.0;//cos(thetaCM)
        st = sqrt(1.0 - ct * ct);//sin(thetaCM)
        //tan(theta1)
        //the angle of incident particle after collision
        tana = st / (ct + mass1 / mass2);
        angle[i] = atan(tana);//theta1
        if (angle[i] < 0.0) angle[i] = angle[i] + PI;
        nuclear_energy_loss[i] = 4.0 * mass1 * mass2 / ((mass1 + mass2) * (mass1 + mass2)) * elab * s2;

        //The distance between the scattering point and the inital target position along the incident direction
        xf[i] = -((mass2 / mass1 - 1.0) * ( coll_para[i] * tancm_half_angle) + 2.0 * time * au) / (mass2 / mass1 + 1.0);
    }

    if (nuclossflag) nuclosscal();
    if (recoilspectraflag) recoilspectra();

    for (i = 0; i < collnum; i++){
        for (j = 0; j < 3; j++) x0[j] = xat[collindex[i]][j] + coll_disp[i][j] - xin[j];
        //When the periodic boundary condition is applied,
        //and the distance is larger than half box size,
        //then do this.
        //Do not understand this part...
        for (j = 0; j < 3; j++)  {
            if (pbc[j] && x0[j] >   prd[j] / 2.0) x0[j] = x0[j] - prd[j];
            if (pbc[j] && x0[j] < - prd[j] / 2.0) x0[j] = x0[j] + prd[j];
        }
        for (j = 0; j < 3; j++) xr[j] = coll_dist[i] * y[j] - x0[j];//Vector of the impact parameter
        for (j = 0; j < 3; j++) xr[j] = xr[j] / coll_para[i];//Unit vector of the impact paramater
        for (j = 0; j < 3; j++) yimpac[i][j] = cos(angle[i]) * y[j] + sin(angle[i]) * xr[j];//Vector along the scattered direction
        yy = 0;
        for (j = 0; j < 3; j++) yy = yy + yimpac[i][j] * yimpac[i][j];
        if (abs(yy - 1.0) > 0.1) printf("warning : ion direction change, see %s line %d \n", __FILE__, __LINE__);
        yy = sqrt(yy);
        for (j = 0; j < 3; j++) yimpac[i][j] = yimpac[i][j] / yy;//Unit vector along the scattered direction
    }

    //** Update the position of the ion
    xtotal = 0.0;
    for (i = 0; i < collnum; i++) xtotal = xtotal + xf[i] + coll_dist[i];
    fl = xtotal / collnum;//average moving distance between two collisions
    if (fl < 0.0) fl = 0.0;

    for (i = 0; i < collnum; i++) energy = energy - nuclear_energy_loss[i];
    if(energy < estop) moving = 0;
    incident -> move(fl);

    //** Update the direciton of the ion
    if (collnum == 1) { for (j = 0; j < 3; j++) y[j] = yimpac[0][j]; }//Unit vector along the scattered direction
    else{
        for (i = 0; i < 3; i++) ytotal[i] = 0.0;
        for (i = 0; i < collnum; i++){
            for (j = 0; j < 3; j++) ytotal[j] = ytotal[j] + yimpac[i][j];
        }
        yy = 0;
        for (i = 0; i < 3; i++) yy = yy + ytotal[i] * ytotal[i];
        yy=sqrt(yy); //Added by JIN.
        for (i = 0; i < 3; i++) y[i] = ytotal[i] / yy;//Unit vector along the scattered direction
    }

    lastcollnum = collnum;
    for (i = 0; i < lastcollnum; i++) lastcollindex[i] = collindex[i];
}

unsigned int Target::sindex(float val){
    /*
     Change the reduced distance to the corresponding row number
     */
    unsigned int ll;
    ll= *(unsigned int *)&val;
    ll = (ll >> SHIFTS)-BIASS;
    if (val < MINS) ll = 0;//Mimimum reduced impact parameter
    if (ll >= DIMS) ll = DIMS - 1;//The matrix dimension
    return ll;
}

unsigned int Target::eindex(float val){
    /*
     Change the reduced energy to the corresponding row number
     */
    unsigned int ll;
    ll = *(unsigned int *)&val;
    ll = (ll >> SHIFTE)-BIASE;//Right Shift Operators,diveded by 2**(SHIFTE)
    if (val < MINE) ll = 0;//Minimum reduced energy
    if (ll >= DIME) ll= DIME - 1;
    return ll;
}

void RBSsimuNS::Target::nuclosscal() {
    /*
     Calculate the nuclear energy loss in each depth bin
     */

    int i, j;
    const double *x = incident -> x;
    const int *image = incident -> image;
    const std::vector<double> &edisplacement = atom -> edisplacement;
    const std::vector<int> &itype = atom -> itype;

    double posz;
    double edis;

    posz = x[2] + image[2] * prd[2];
    i = floor (posz / depthstep + 0.5);
    if (i < 0 || i > nucdepthnum) return;
    for (j = 0; j < collnum; j++){
        edis =  edisplacement[itype[collindex[j]]];
        nucloss[i] = nucloss[i] + nuclear_energy_loss[j];
        if (nuclear_energy_loss[j] >= 2.0 * edis ){
            ndisp[i] = ndisp[i] + nuclear_energy_loss[j] / (2.0 * edis);
        }
        else if(nuclear_energy_loss[j] < 2.0 * edis && nuclear_energy_loss[j] > edis){
            ndisp[i] = ndisp[i] + 1.0;
        }
        else continue;
    }
}

void RBSsimuNS::Target::loc2DensityCheck(double x, double y, double z, double &den, double &aDen){
    /*
    Test if the ion is located in the local box;
    update the local density and atomic density.

    If multiple same-shape-voids overlap, the value of the first-readed void will be used;
    If multiple different-shape voids overlap, the order of preference: Box > Sphere.
     */

    //Find the position of the ion by moving it back to the initial target
    if ((x - xlo[0]) >= 0) x = fmod((x - xlo[0]), prd[0]) + xlo[0];
    else x = (xlo[0] + prd[0]) + fmod((x - xlo[0]), prd[0]);
    if ((y - xlo[1]) >= 0) y = fmod((y - xlo[1]), prd[1]) + xlo[1];
    else y = (xlo[1] + prd[1]) + fmod((y - xlo[1]), prd[1]);
    if ((z - xlo[2]) >= 0) z = fmod((z - xlo[2]), prd[2]) + xlo[2];
    else z = (xlo[2] + prd[2]) + fmod((z - xlo[2]), prd[2]);

    for (int i = 0; i < locBox.size(); i++) {
        if (x >= locBox[i][0] && x < locBox[i][1] && y >= locBox[i][2] && y < locBox[i][3] && z >= locBox[i][4] && z < locBox[i][5]) {
            den = locBox[i][6];
            aDen = locBox[i][7];
            return;
        }
    }

    for (int i = 0; i < locSphere.size(); i++) {
        if (pow(x - locSphere[i][0], 2) + pow(y - locSphere[i][1], 2) + pow(z - locSphere[i][2], 2) < pow(locSphere[i][3], 2)) {
            den = locSphere[i][4];
            aDen = locSphere[i][5];
            return;
        }
    }

    den = parameter -> density;
    aDen = atom -> ndensity;

}

void RBSsimuNS::Target::printdepthnuc(const int &num) {
    /*
     Print total nuclear energy loss per depth
     and total displacements per depth
     */

    int i;
    int nprint = 40;

    std::ofstream nucout;

    if ((num + 1) % nprint == 0 || (num + 1) == incident -> nincident){
        nucout.open("out/nuclear_depthprofile.dat", std::ios::out);
        if(!nucout){
            printf("Cannot open the out/nuclear_depthprofile.dat file\n");
        }
        else{
            for (i = 0; i < nucdepthnum; i++){
                nucout << std::setw(10) << std::fixed << depth[i] << "  " << nucloss[i] / depthstep << std::endl;
            }
            nucout.close();
        }

        nucout.open("out/damage.dat", std::ios::out);
        if(!nucout){
            printf("Cannot open the out/damage.dat file\n");
        }
        else{
            for (i = 0; i < nucdepthnum; i++){
                nucout << std::setw(10) << std::fixed << depth[i] << "  " << ndisp[i] / depthstep << std::endl;
            }
            nucout.close();
        }
    }
}

void RBSsimuNS::Target::recoilspectra() {
    /*
     Calculate the total recoil energy in a certain energy region:  recspec[i]
     Calculate the toatl recoil numbers in a certain energy region: irecspec[i]
     Note:
     The value may be overestimated due to "if (nuclear_energy_loss[j] > exp (- 0.5 / drecspec))"
     */

    int i, j;
    const double *x = incident -> x;
    const int *image = incident -> image;
    const double &size = parameter -> depthsize;
    double posz;

    posz = x[2] + image[2] * prd[2];

    if (posz > 0 && posz < size){
        for (j = 0; j < collnum; j++){
            if (nuclear_energy_loss[j] > exp (- 0.5 / drecspec)){
                i = (int) (log (nuclear_energy_loss[j]) * drecspec + 0.5);
                if (i >= recspecsize) i = recspecsize - 1;
                if (i > maxrecsize) maxrecsize = i;
                recspec[i] = recspec[i] + nuclear_energy_loss[j];
                irecspec[i] = irecspec[i] + 1.0;
            }
        }
    }
}

void RBSsimuNS::Target::printrecoilspectra(const int &num) {
    /*
     Print:
     Recoil energy: rece[i];
     Recoil number per unit energy per incident ion: irecspec[i] / de / (num + 1.0);
     Recoil number per incident ion: irecspec[i]/(num + 1.0);
     Recoil energy per incident ion: recspec[i] / (num + 1.0);
     Total recoil number: irecspec[i]
     */

    int i;
    int nprint = 40;
    double de;

    FILE *fp;

    if ((num + 1) % nprint == 0 || (num + 1) == incident -> nincident){
        fp = fopen("out/recspectra.dat", "w");
        if(!fp){
            printf("can not open the out/recspectra.dat file\n");
        }
        else{
            for (i = 0; i < maxrecsize + 1; i++){
                de = exp((i + 0.5) /drecspec) - exp((i - 0.5) / drecspec); //Recoil energy range
                fprintf(fp, "%10.2lf %12.6lf  %10.3lf %10.4lf %10.6lf %10.0lf\n", rece[i], irecspec[i] / de / (num + 1.0), de, irecspec[i]/(num + 1.0), recspec[i] / (num + 1.0), irecspec[i]);
            }
            fclose(fp);
        }
    }
}
