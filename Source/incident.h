#ifndef REBUILD_INCIDENT_H
#define REBUILD_INCIDENT_H

#include <vector>
#include <string>

#include "pointer.h"

namespace RBSsimuNS {

    class Incident : public Pointer {
    public:

        double                      z, mass, estop;
        int                         lastion;
        int                         nincident;
        std::vector<double>         pathrange;
        std::vector<std::vector<double> >   xinpos;
        std::vector<std::vector<double> >   xfipos;
        std::vector<double>         xfinal; //Abj
        std::vector<double>         yfinal; //Abj
        std::vector<double>         cosfivel;

        double                      penetrationpowerstep;
        int                         numdepth;
        std::vector<double>         enperstep;
        std::vector<double>         totalenergyperstep;
        std::vector<int>            timesperstep;

        std::vector<double>         maximpact;

        std::string                 stopPath;
        std::vector<double>         v_loss;
        std::vector<double>         e_loss;

        int                         into;
        double                      x[3], y[3];
        double                      y_init[3];
        double                      energy;
        int                         moving;
        int                         image[3];

        double                      prange;

        int                         rotate_mode;
        int                         IBD_mode;

        Incident(RBSsimu *rbs);
        ~Incident();

        void simu(const int rangeflag, const int rbsflag, const int rbscountflag, const int nuclossflag, const int recoilspectraflag, const int finalangleflag,
                  const int penetrationpowerflag, const int pathflag, const int locDenflag, const int locDenflag2, const int nraflag);
        void init();

        void max_impact_parameter(); //Added by JIN
        void print_details(); //Added by JIN
        double Newton_method(double r0, const int z1, const int z2, const double V0, const double epsilon, const int max_iter); //AbJ

        void read_ele_energyloss();

        void ion_init();
        void pre_rotation_matrix(const double theta, const double fii, const double *indirec, double *outdirec);
        void move(const double fl);
        const double eloss(const double v1);
        const double LSSomega(const double v1, const double fl);

        void range(const int &flag);
        void velocity_directions();
        void penetrationpowerstatis();
        void penetrationpoweraccumu(const int &nions);
        void printtime(const double &t1, const double &t2); //Added by JIN
        void final_xy(const int &num);
    };

}


#endif 
