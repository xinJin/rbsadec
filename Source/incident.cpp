#include <cstdio>
#include <memory>
#include <iostream> 
#include <string>
#include <cmath> 
#include <fstream> 
#include <sstream> 
#include <iomanip>
#include <mpi.h>
//#include <unistd.h> //For sleep
//#include <sys/time.h> //For resources usage
//#include <sys/resource.h> //For resources usage

#include "incident.h"
#include "rbssimu.h"
#include "atom_type.h"
#include "parameter.h"
#include "atom.h"
#include "target.h"
#include "nrasimu.h"

using namespace RBSsimuNS;

RBSsimuNS::Incident::Incident(RBSsimu *rbs):Pointer(rbs) {
    
}

RBSsimuNS::Incident::~Incident() {
    
}

void RBSsimuNS::Incident::init() {
    /*
     Obtain the projectile nature;
     Obtain the total simulation history;
     Set the incident position of each projectile (randomly);
     Initialize some parameters.
     */

    int i,j;
    double tmp;

    std::vector<double> posmin(parameter -> positionmin, parameter -> positionmin + 3);
    std::vector<double> posmax(parameter -> positionmax, parameter -> positionmax + 3);
    std::string ion = parameter -> ionsymbol;

    i = atom_type -> value_atom_type(ion, z, mass, tmp, estop);
    if (i == 0) {
        printf("can not find the projectile atom type %s, check !! \n", ion.c_str());
        exit(1);
    }

    estop = parameter->ThresholdEnergy; //Added by JIN
    lastion = -1;

    nincident = parameter -> numhis;
    pathrange.resize(nincident, 0.0);
    xinpos.resize(nincident, std::vector<double>(3, 0));//initial projectile position
    xfipos.resize(nincident, std::vector<double>(3, 0));//final projectile position
    xfinal.resize(1, 0);//Final x coordinate of the ion
    yfinal.resize(1, 0);//Final y coordinate of the ion
    cosfivel.resize(nincident, 0);

    for (i = 0; i < nincident; i++){
        for (j = 0; j < 3; j++) {
            xinpos[i][j] = posmin[j] + (posmax[j] - posmin[j]) * rbssimu -> rand();
        }
    }

    penetrationpowerstep = 2.0 * parameter -> binsize[2];
    numdepth = parameter -> depthstop / penetrationpowerstep;
    enperstep.resize(numdepth, 0.0);
    totalenergyperstep.resize(numdepth, 0.0);
    timesperstep.resize(numdepth, 0);

    prange = 0.0; //AbJ
}

void RBSsimuNS::Incident::ion_init() {
    /*
     Set up the projectile initial moving direction;
     Calculate the projectile moving direction after the rotation.

     New things (AbJ)
     Rotation mode:
     if 0, rotate the incident ion azimuthal angle according to parameter->rotafii;
     if 1, rotate the incident ion azimuthal angle from 0 degree to 359 degree
     with 1 degree per ion as interval.
     if 2, in addition to the operation in mode 1, the detector angle will change as the
     incident angle changes, in order to make sure that the angle between the incident direction
     and the detector direction is a fixed value.
     IBD mode:
     if 0, isotropic from thetamin to thetamax (The original method);
     if 1, theta follows Gaussian distribution, with thetamin as the mean
     and with thetamax as the standard deviation;
     if 2, the spatial distribution of projectile on the target surface
     is Gaussian, with thetamax related with the standard deviation
     (provided the thetamax is small).
     */

    static int iincident = 0;
    int i;
    double yi[3];
    double fii, theta;

    double thetamin = parameter -> thetamin / 180.0 * PI;
    double thetamax = parameter -> thetamax / 180.0 * PI;
    double rotheta = parameter -> rotatheta / 180.0 * PI;
    double rofii = 0;

    into = 0;
    moving = 1;
    for (i = 0; i < 3; i++) image[i] = 0;

    if(0 == rotate_mode) {
        rofii = parameter->rotafii / 180.0 * PI;
        rbssimu -> detectortheta = parameter -> detectortheta;
        rbssimu -> detectorfii = parameter -> detectorfii;
    }
    else if(1 == rotate_mode) {
        rofii = iincident % 360 / 180.0 * PI;
        rbssimu -> detectortheta = parameter -> detectortheta;
        rbssimu -> detectorfii = parameter -> detectorfii;
    }
    else if(2 == rotate_mode) {
        rofii = iincident % 360 / 180.0 * PI;
        rbssimu -> detectortheta = parameter -> detectortheta + parameter -> rotatheta;
        if(rbssimu -> detectortheta > 0) rbssimu -> detectorfii = rofii / PI * 180;
        else rbssimu -> detectorfii = parameter -> detectorfii;
    }
    else{
        rofii = parameter->rotafii / 180.0 * PI;
        rbssimu -> detectortheta = parameter -> detectortheta;
        rbssimu -> detectorfii = parameter -> detectorfii;
    }

    if(rbssimu -> detectortheta > 180){
        printf("Warning: the polar angle of the detector is larger than 180 degree.\n");
        exit(1);
    }

    fii = parameter -> fiimin + (parameter -> fiimax - parameter -> fiimin) * rbssimu -> rand();
    if(0 == IBD_mode){
        theta = acos(cos(thetamin) - (cos(thetamin) - cos(thetamax)) * rbssimu -> rand());
    }
    else if(1 == IBD_mode){
        theta = thetamin + (thetamax - thetamin) * rbssimu -> gaussianrand();
    }
    else if(2 == IBD_mode){
        theta = thetamin + sqrt(-2*thetamax*thetamax*log(1-rbssimu->rand()));
    }
    else{
        theta = acos(cos(thetamin) - (cos(thetamin) - cos(thetamax)) * rbssimu -> rand());
    }
    if (theta<0) theta = -1 * theta;

    //projection to x, y and z
    yi[0] = sin(theta) * cos(fii);
    yi[1] = sin(theta) * sin(fii);
    yi[2] = cos(theta);

    pre_rotation_matrix(rotheta, rofii, yi, y);
    for (i = 0; i < 3; i++) y_init[i] = y[i];
    energy = parameter -> energy;
    for (i = 0; i < 3; i++) x[i] = xinpos[iincident][i];

    for (i = 0; i < numdepth; i++){
        timesperstep[i] = 0;
        enperstep[i] = 0.0;
    }

    iincident++;
}

void RBSsimuNS::Incident::pre_rotation_matrix(const double theta, const double fii, const double *indirec,
                                              double *outdirec) {
    /*
     Rotate a 3D vector in Cartesian coordinates
     according to the polar and azimuthal angle needed to be rotated.
     */

    int i,j;
    double ct,st,cf,sf;

    ct = cos(theta);
    st = sin(theta);
    cf = cos(fii);
    sf = sin(fii);

    double s[3][3] = { {cf * ct, -sf, cf * st},
                       {sf * ct,  cf, sf * st},
                       {-st    , 0.0, ct     }};

    for(i = 0; i < 3; i++) outdirec[i] = 0.0;

    for (i = 0; i < 3; i++) {
        for (j = 0; j< 3; j++)
            outdirec[i] = outdirec[i] + s[i][j] * indirec[j];//outdirec[i] is y[i]
    }
}

void RBSsimuNS::Incident::read_ele_energyloss() {
    /*
     Read the (electronic) stopping power data.
     The first column is the velocity (m/s);
     The second column is the (electronic) stopping power (eV/Angstrom).
     */

    double tmp1, tmp2;
    std::string line;
    std::stringstream ss;
    std::ifstream readStop;

    readStop.open(stopPath);
    if(!readStop){
        std::cerr << "Oh no," << stopPath <<  " could not be opened for reading!" << std::endl;
        exit(1);
    }

    while(readStop){
        ss.clear();
        getline(readStop, line);
        ss.str(line);
        if(ss >> tmp1 && ss >> tmp2){
            v_loss.push_back(tmp1);
            e_loss.push_back(tmp2);
        }
    }
    readStop.close();
}

const double RBSsimuNS::Incident::eloss(const double v1) {
    /*
     Return the electronic energy loss [ev/Angstrom] at velocity v1
     */

    int i, ihalf, n;
    static int iup = 0, idown = 0;
    double vreal, Se;
    double scale;

    vreal=v1;
    n=v_loss.size();

    /*
      Get two nearest elements with binary search
   */
    if (vreal > v_loss[n-1]) {
        printf("Error: In the stopping power calculation, the velocity of the ion is %lf large than the maximum one %lf in input file\n", vreal, v_loss[n-1]);
        exit(1);
    }
    if (vreal < v_loss[0]) {
        printf("Error: In the stopping power calculation, the velocity of the ion is %lf smaller than the minimum one %lf in input file\n", vreal, v_loss[0]);
        exit(1);
    }

    if (!(vreal > v_loss[idown] && vreal < v_loss[iup] && (iup - idown) == 1 )){
        iup = n-1; idown = 0;
        while (1) {
            ihalf = idown + (iup - idown) / 2;
            if (ihalf == idown) break;
            if (v_loss[ihalf] < vreal) idown = ihalf;
            else iup = ihalf;
        }
    }

    scale = 1.0;
    /* Get elstop with a simple linear interpolation */
    Se = e_loss[idown] * (v_loss[iup] - vreal) / (v_loss[iup] - v_loss[idown]);
    Se += e_loss[iup] * (vreal - v_loss[idown]) / (v_loss[iup] - v_loss[idown]);
    Se *= scale;

    return(Se);
}

const double RBSsimuNS::Incident::LSSomega(const double v1, const double fl) {
    /*
     Calculate the energy straggling
     */

    const double &z1 = z;
    const double &z2 = atom -> meanz;
    const double &n = atom -> ndensity;

    double omega, x, L;
    double bohrvel = 2187700.0;//Bohr velocity=e*e/(4*pi*eps0*hbar)
    double e = 1.602189e-19;
    double eps0 = 8.854188e-12;//permittivity of free space
    double angstrom = 1e-10;

    //unit of omega is eV, fl is distance, omega represents Bohr electronic energy loss straggling
    //(near Gaussian distribution)
    //omega increases with increasing fl
    //the angstrom should be unitless
    omega = (z1 * e / (4.0 * PI * eps0) * sqrt(4.0 * PI * z2 * n * fl / (angstrom * angstrom)));

    //Bohr electronic energy loss breaks down for low and medium energies, Lindhard and Scharff applied a correction factor L
    //The x should be 3 for applying the correction factor, according to some literatures.
    x = v1 * v1 / (bohrvel * bohrvel * z2);

    L = 1.0;

    if (x < 2.3) L = 0.5 * (1.36 * sqrt(x) - 0.016 * sqrt(x * x * x));

    return (omega * sqrt(L));
}

void RBSsimuNS::Incident::move(const double fl) {
    /*
     Calculate the new location of the projectile after the movement.
     If the periodic boundary condition is applied, the image of the new target is calculated.
     Calculate the energy loss.

     Stop the projectile:
     When the periodic boundary condition is not applied and the projectile gets off from the box;
     When the projectile moves backwards and the location in z axis is smaller than the box minimum z-boundary;
     When the projectile z-location is larger than the maximum depth;
     When the projectile energy is smaller than the minimum energy.

     New things (AbJ)
     locDenflag: A flag for taking into account the local atomic density variation which will
                 have effect on the calculation of electronic energy loss.
     If locDenflag = 0:
                  original method;
     If locDenflag = 1:
                  1) check the number of bin where the ion is currently located;
                  2) calcualte the ratio of local atomic density to the global atomic density;
                  3) multiply the electronic energy loss with this local density ratio.

     Note: The movement along the z direction should be treated separately.
     If, after the movement, the projectile has still not entered into the target
     and the periodic boundary condition is applied, the projecile will move to
     the end of the target box. By this way, in Target::find_check_atom(),
     when into == 0, head[ix * (nbiny * nbinz) + iy * nbinz + iz] can be out of the bound.

     Note: If the boxsize is too small, the image calculation may give an underestimated result (potential problem).
     */

    int i;
    double energyloss, v, omega;
    int binCount; //Abj
    const int &mode = parameter -> elstopmode;//mode of Energy Loss Straggle
    const int locDenflag = parameter -> locDenflag; //Abj
    const int locDenflag2 = parameter -> locDenflag2; //Abj
    const std::vector<int> pbc(target -> pbc, target -> pbc + 3);
    const std::vector<double> boxsize(target -> prd, target -> prd + 3);
    const std::vector<double> xlo(target -> xlo, target -> xlo + 3); //AbJ
    const std::vector<double> binsize_den = target -> binsize_den; //Abj
    const std::vector<int> binnum_den = target -> binnum_den; //Abj
    const std::vector<double> &binDen = target -> binDen; //Abj
    double locDen, locADen; //Abj
    const double &depthstop = parameter -> depthstop;//Maximum Depth of simulation

    double e = 1.6021892e-19;
    double u = 1.6605655e-27;

    v = sqrt (2.0 * energy * e / (mass * u));

    if(std::isnan(v)) {
        printf("The velocity has problem! See %s line %d \n", __FILE__, __LINE__);
        exit(1);
    }

    if(0 == locDenflag && 0 == locDenflag2) {
        energyloss = eloss(v) * fl;
    }
    else if (0 == locDenflag && 1 == locDenflag2) {
        //Check if the ion is in the empty zone
        target -> loc2DensityCheck(x[0], x[1], x[2], locDen, locADen);
        energyloss = eloss(v) * fl * (locDen / parameter -> density);
    }
    else if(1 == locDenflag && 0 == locDenflag2 && 0 == moving){
        //While moving is 0, there is the possibility of bound limit overflow in binNumber,
        //thus the original method is used here.
        energyloss = eloss(v) * fl;
    }
    else if(1 == locDenflag && 0 == locDenflag2 && moving != 0){
        std::vector<int> idbin(3, 0);
        int binExceed = 0;

        for(int j = 0; j < 3; j++){
            idbin.at(j) = (floor) ((x[j] - xlo[j]) / binsize_den.at(j) );
            if (idbin.at(j) < 0 && pbc[j] != 0) idbin.at(j) = (idbin.at(j) % binnum_den.at(j)) + binnum_den.at(j);
            if (idbin.at(j) >= binnum_den.at(j) && pbc[j] != 0) idbin.at(j) = idbin.at(j) % binnum_den.at(j);
            if (pbc[j] == 0 && idbin.at(j) < 0) binExceed = 1;
            if (pbc[j] == 0 && idbin.at(j) >= binnum_den.at(j))  binExceed = 1;
        }

        if (1 == binExceed) {
            /*
             If the pbc is 0, it is possible that the bin number would exceed the limit.
             In this case, the original method is used, instead of calculating the bin number.
             Potential problem: at this case, it is better to have no electronic energy loss.
             */
            energyloss = eloss(v) * fl;
        }
        else {
            binCount = idbin.at(0) * (binnum_den.at(1) * binnum_den.at(2))
                       + idbin.at(1) * binnum_den.at(2) + idbin.at(2);

            if (binCount < 0 || binCount >= binnum_den[0] * binnum_den[1] * binnum_den[2]) {
                //Bound limit overflow happened here!
                printf("Warning, bound limit overflow, UB! see %s line %d \n", __FILE__, __LINE__);
                printf("Bin_DEN size (x, y, z): %.4f %.4f %.4f\n", binsize_den[0], binsize_den[1], binsize_den[2]);
                printf("Bin_DEN Number (x, y, z): %.d %.d %.d\n", binnum_den[0], binnum_den[1], binnum_den[2]);
                printf("Ion position (x, y, z): %.4f %.4f %.4f\n", x[0], x[1], x[2]);
                printf("Ion energy: %.4f\n", energy);
                printf("Bin_DEN position (x, y, z): %.d %.d %.d\n", idbin[0], idbin[1], idbin[2]);
                printf("Current bin number: %d\n", binCount);
                exit(1);
            }

            energyloss = eloss(v) * binDen.at(binCount) * fl;;
        }

    }
    else if (locDenflag == 1 && locDenflag2 == 1) {
        printf("The value of locDenflag and locDenflag2 cannot be both 1.\n");
        exit(1);
    }
    else{
        printf("The value of locDenflag (%d) or locDenflag2 (%d) has problem.\n", locDenflag, locDenflag2);
        exit(1);
    }

    //Modified by JIN
    /*
     Original:
    for (i = 0; i < 3; i++){
        x[i] = x[i] + fl * y[i];
        if (pbc[i] && x[i] >= boxsize[i]) { x[i] = x[i] - boxsize[i]; image[i] = image[i] + 1; }
        if (pbc[i] && x[i] < 0.0) { x[i] = x[i] + boxsize[i]; image[i] = image[i] - 1; }
        if(!pbc[i] && (x[i] >= boxsize[i] || x[i] < 0.0) && into == 1) moving = 0.0;
    }
     */
    /*
    Potential problems:
     If the moving distance is larger than boxsize[i], then the ion is out of the box.
     */
    for (i = 0; i < 2; i++){
        x[i] = x[i] + fl * y[i];
        if (pbc[i] && x[i] >= (xlo[i] + boxsize[i])) { x[i] = x[i] - boxsize[i]; image[i] = image[i] + 1; }
        if (pbc[i] && x[i] < xlo[i]) { x[i] = x[i] + boxsize[i]; image[i] = image[i] - 1; }
        if(!pbc[i] && (x[i] >= (xlo[i] + boxsize[i]) || x[i] < xlo[i]) && into == 1) moving = 0.0;
    }
    x[2] = x[2] + fl * y[2];
    if (pbc[2] && x[2] >= (xlo[2] + boxsize[2])) { x[2] = x[2] - boxsize[2]; image[2] = image[2] + 1; }
    if (pbc[2] && x[2] < xlo[2] && into == 1) { x[2] = x[2] + boxsize[2]; image[2] = image[2] - 1; }
    if(!pbc[2] && (x[2] >= (xlo[2] + boxsize[2]) || x[2] < xlo[2]) && into == 1) moving = 0.0;

    if (x[2] + boxsize[2] * image[2] < xlo[2] && y[2] < 0.0) moving = 0;
    if (x[2] + boxsize[2] * image[2] > depthstop) moving = 0;

    if (into == 0) return;

    if (mode == 0)   energy = energy - energyloss;
    else {
        omega = LSSomega(v, fl);
        energy = energy - (energyloss + omega * rbssimu -> gaussianrand());
    }

    if (energy < estop) { moving = 0; }
}

void RBSsimuNS::Incident::simu(const int rangeflag, const int rbsflag, const int rbscountflag, const int nuclossflag,
                               const int recoilspectraflag, const int finalangleflag, const int penetrationpowerflag,
                               const int pathflag, const int locDenflag, const int locDenflag2, const int nraflag){
    /*
     Perform the simulation.
     */

    clock_t t0, t1, tb, tf;
    tb = clock();
    double  t = 0.0, ttotal = 0.0;
    int i;

    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    if (locDenflag) parameter -> locDenflag = 1; //AbJ
    if (locDenflag2) parameter -> locDenflag2 = 1; //Abj
    if (nraflag) parameter -> nraflag = 1; //Abj

    parameter -> read_parameter();

    atom_type -> Atom_type_read();

    rbssimu -> init();

    atom -> init();
    atom -> read_coordinate();
    atom -> read_scatter_matrix();
    atom -> read_time_matrix();

    target -> init();
    target -> bin();

    if (nraflag) nra -> init();

    init();

    max_impact_parameter();//AbJ

    read_ele_energyloss();

    if (rbscountflag) rbssimu -> countflag = 1;
    if (nuclossflag) target -> nuclossflag = 1;
    if (recoilspectraflag) target -> recoilspectraflag = 1;

    int ionNumberPerProc = parameter -> ionNumberPerProc;
    int ionNumberPerProcOdd = parameter -> ionNumberPerProcOdd;

    for (i = 0; i < ionNumberPerProc; i++) {

        //printf("Ion %d\n\n", i);

        if (i >= nincident) {
            printf("Error: the total number of incident ions overflows\n");
            exit(1);
        }

        if (0 == world_rank) {
            if(0 == i) printf("\nThe ions start to bombard the target!\n");

            if ((i + 1) % 100 == 0){
                printf ("\r%.3lf %s simulation finished.", (i + 1.0) / ionNumberPerProc * 100.0, "%");
                fflush(stdout);
            }

            if ( (i + 1) * 10 % ionNumberPerProc == 0 ) printf("\n");
        }

        ion_init();

        while(moving){

            target -> find_check_atom();
            t0 = clock();
            target -> find_coll_partner();
            t1 = clock();
            t = t + (double) (t1 - t0);
            if (rbsflag) rbssimu -> emit();
            if (nraflag) nra -> emit();
            target -> collision();
            if (penetrationpowerflag) penetrationpowerstatis();
        }

        if (rangeflag) range(0);
        if (rbsflag) rbssimu -> printrbs(i);
        if (nraflag) nra ->printNRA(i, ionNumberPerProc, ionNumberPerProcOdd);
        if (rbscountflag) rbssimu -> printdepthrbsnum(i);
        if (nuclossflag) target -> printdepthnuc(i);
        if (recoilspectraflag) target -> printrecoilspectra(i);
        if (finalangleflag) velocity_directions();
        if (pathflag) final_xy(i);
        if (penetrationpowerflag) penetrationpoweraccumu(i);
    }

    if (0 == world_rank) {
        print_details();

        tf = clock();
        ttotal = (double) (tf - tb) / CLOCKS_PER_SEC;
        printf("\nFind partner spend %lf s\n", t / CLOCKS_PER_SEC);
        printf("All simu spend %lf s\n", ttotal);

        printtime(t / CLOCKS_PER_SEC, ttotal);
    }
}

void RBSsimuNS::Incident::range(const int &flag) {
    /*
     Calculate the mean, the standard deviation and the standard deviation of mean;
     Print the range.
     */

    static int nstop = 0;
    int noutput = 40;
    double zminimum = 0;
    const double *boxsize = target -> prd;

    const int maxrnum = 1000;
    int count[maxrnum];
    int rangenum;
    double range[nstop + 1];

    int i, j, jmax;
    double min, minr, max, sum, r, mean, deltar, xi;
    double dmean, sigmar, straggle;

    std::ofstream rangeout;

    for (i = 0; i < 3; i++) xfipos[nstop][i] = x[i] + image[i] * boxsize[i];
    pathrange[nstop] = prange;
    nstop++;

    if ((nstop + 1) % noutput == 0 || (nstop + 1) == nincident){//MbJ
        j = 0;
        if (flag == 0){
            for (i = 0; i < nstop; i++) {
                if (xfipos[i][2] > zminimum) {
                    range[j] = xfipos[i][2];
                    j++;
                }
            }
            rangenum = j;
        }
        if (flag == 1){
            for (i = 0; i < nstop; i++)  range[i] = pathrange[i];
            rangenum = nstop;
        }

        for (i = 0; i < maxrnum; i++) count[i] = 0;
        min = 1e20; max = -1e20; sum = 0;
        mean = 0; sigmar = 0.0; straggle = 0.0;

        for (i = 0; i < rangenum; i++){
            r = range[i] ;
            if(min > r) min = r;
            if(max < r) max = r;
            sum = sum + r;
        }
        if (rangenum > 0) mean = sum / rangenum;

        deltar = ((mean - min) > (max - mean) ? (max - mean) : (mean - min)  )/15.0;
        if (deltar <= 1e-10 || deltar >= 1e10 ) {
            printf (" Warning : deltar  %f min  %f mean %f  max  %f \n", deltar, min, mean, max);
            deltar = 1.0 ;
        }

        xi = pow (10.0 , floor (log10 (deltar)));
        deltar = xi * floor ((deltar + xi / 2.0) / xi);
        minr = xi * floor (min / deltar);

        dmean = 0.0;
        jmax = 1;
        for (i = 0 ; i < rangenum; i++){
            dmean = dmean + (range[i] - mean )* (range[i] - mean );
            r = range[i] + deltar / 2.0 - minr;
            j = floor (r / deltar);
            if ( j >= maxrnum - 2 ){
                printf(" Warning : range outsiede range array \n");
                j = maxrnum - 3 ;
            }
            if (j < 0 ) {
                printf(" Warning : the range small than min r(should not be)!\n");
                j = 0;
            }
            if (j > jmax ) jmax = j;
            count[j]++;
        }

        if (rangenum > 1){
            straggle = sqrt(dmean / rangenum);
            sigmar = sqrt (dmean / (rangenum - 1) / rangenum);
            printf("%d incident ions, range %lf +- %lf, straggle %lf\n", rangenum + 1, mean,sigmar, straggle);
        }

        rangeout.open("out/range.dat", std::ios::out);
        if(!rangeout){
            printf("Cannot open the out/range.dat file \n");
            rangeout.close();
        }
        else{
            for (i = 0; i < jmax + 2; i++){
                rangeout << std::setw(20) << std::right << std::fixed << std::setprecision(2) << minr + i * deltar << "   " << count[i] << std::endl;
            }
            rangeout.close();
        }
    }
}

void RBSsimuNS::Incident::velocity_directions() {
    /*
     Print the final polar angle distribution
     */

    int i;
    int num = 18000;
    static int nstop = 0;
    int noutput = 100;
    int count[num];
    int j;
    int ionNumberPerProc = parameter -> ionNumberPerProc;
    int ionNumberPerProcOdd = parameter -> ionNumberPerProcOdd;
    int numCount = 0;

    std::ofstream velocout;

    cosfivel[nstop] = y[2] * 1.0;
    nstop++;

    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    if ((nstop + 1) % noutput == 0 || (nstop + 1) == ionNumberPerProcOdd || (nstop + 1) == ionNumberPerProc){

        std::vector<double> cosfivelOut(nincident, 0);
        std::vector<double> cosfivelBundle(nincident, 0);

        if (0 == world_rank) cosfivelBundle.resize(nincident * world_size, 0);

        //Gather cosfivel data from all the processors;
        //Update cosfivel data and nstop in processor_0
        //cosfivelBundle: [6,7,0,...,0, 5,4,0,...,0, 3,4,0,...,0]
        //                 From Proc0   From Proc1   From Proc2
        //cosfivel or cosfivelOut:
        //                 [6,7, 5,4, 3,4, 0,...,0]
        if (nstop < ionNumberPerProcOdd) {
            MPI_Barrier(MPI_COMM_WORLD);
            MPI_Gather(cosfivel.data(), cosfivel.size(), MPI_DOUBLE, cosfivelBundle.data(), cosfivel.size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);

            if ( (nstop + 1) == ionNumberPerProcOdd) {
                numCount = nstop;
                if (0 == world_rank) {
                    for (int i = 0; i < world_size; i++) {
                        for (int j = 0; j < numCount + 1; j++) {
                            cosfivel[j + numCount * i] = cosfivelBundle[j + nincident * i];
                            if (i > 0) nstop++;
                        }
                    }
                }
            }

        }

        //Get cosfivelOut data (to be printed)
        if (0 == world_rank) {
            if (nstop < ionNumberPerProcOdd) {
                for (int i = 0; i < world_size; i++) {
                    for (int j = 0; j < nstop + 1; j++) {
                        cosfivelOut[j + nstop * i] = cosfivelBundle[j + nincident * i];
                    }
                }
            }
            else {
                for (int i = 0; i < cosfivel.size(); i++) cosfivelOut[i] = cosfivel[i];
            }
        }

        //To print
        if (0 == world_rank) {
            for (i = 0; i < num; i++) count[i] = 0;
            for (i = 0; i < nincident; i++){
                j = (int) (acos(cosfivelOut[i]) / 3.14 * 180.0 * 100.0 + 0.5);
                count[j]++;
            }

            velocout.open("out/final_direction.dat", std::ios::out);
            if(!velocout){
                printf("Cannot open the out/final_direction.dat file \n");
            }
            else {
                for (i = 0; i < num; i++) {
                    velocout << std::fixed << i * 0.01 << "  " << count[i] << std::endl;
                }
                velocout.close();
            }
        }

    }
}

void RBSsimuNS::Incident::final_xy(const int &num) {
    /*
     Print the final x and y positions of the ions
     */

    int nprint = 100;
    const double *boxsize = target -> prd;
    int ionNumberPerProc = parameter -> ionNumberPerProc;
    int ionNumberPerProcOdd = parameter -> ionNumberPerProcOdd;

    std::ofstream out;

    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    //Get the x and y positions of the ions at the end of their history
    xfinal.push_back(x[0] + image[0] * boxsize[0]);
    yfinal.push_back(x[1] + image[1] * boxsize[1]);

    if ((num + 1) % nprint == 0 || (num + 1) == ionNumberPerProcOdd || (num + 1) == ionNumberPerProc) {

        std::vector<double> xfOut, yfOut;
        std::vector<double> xfBundle(xfinal.size(), 0), yfBundle(yfinal.size(), 0);

        if (0 == world_rank) {
            if (xfinal.size() != yfinal.size()) {
                printf("Error: the final 2D positions of ions have problem. See %s line %d \n", __FILE__, __LINE__);
                exit(1);
            }

            xfBundle.resize(xfinal.size() * world_size, 0);
            yfBundle.resize(yfinal.size() * world_size, 0);
        }

        //Gather xfinal/yfinal data from all the processors;
        if (num < ionNumberPerProcOdd) {
            MPI_Barrier(MPI_COMM_WORLD);
            MPI_Gather(xfinal.data(), xfinal.size(), MPI_DOUBLE, xfBundle.data(), xfinal.size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
            MPI_Gather(yfinal.data(), yfinal.size(), MPI_DOUBLE, yfBundle.data(), yfinal.size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
        }

        //Get xfOut/yfOut data from xfBundle/yfBundle or xfinal/yfinal(to be printed)
        //xfBundle: [0,4,5,4,3, 0,6,7,6,9, 0,9,5,4,7] (the 0s are from the initialization of vectors)
        //xfOut: [4,5,4,3, 6,7,6,9, 9,5,4,7]
        if (0 == world_rank) {
            if (num < ionNumberPerProcOdd) {
                for (int i = 0; i < xfBundle.size(); i++) {
                    if (i % xfinal.size() == 0) continue;
                    xfOut.push_back(xfBundle[i]);
                    yfOut.push_back(yfBundle[i]);
                }
            }
            else {
                for (int i = 0; i < xfinal.size(); i++) {
                    if (i % xfinal.size() == 0) continue;
                    xfOut.push_back(xfinal[i]);
                    yfOut.push_back(yfinal[i]);
                }
            }
        }

        //To print
        if (0 == world_rank) {
            out.open("out/final_xy.dat", std::ios::out | std::ios::app);
            if(!out){
                printf("Cannot open or append the out/final_xy.dat file \n");
            }
            else {
                for (int i = 0; i < xfOut.size(); i++) {
                    out << std::fixed << std::setprecision(2) << xfOut[i] << "  " << yfOut[i] << std::endl;
                }
                out.close();
            }
        }

        //Re-initialize xfinal & yfinal
        xfinal.resize(1, 0);
        yfinal.resize(1, 0);
    }

}

void RBSsimuNS::Incident::penetrationpowerstatis() {
    /*
     Calculate the total penetrating ion and the associated energy
     in each depth bin
     */

    int i;
    double posz;
    const double *prd = target -> prd;

    posz = x[2] + image[2] * prd[2];
    i = (int) (posz / penetrationpowerstep); 
    if (i >= numdepth || i < 0) return;
    if (y[2] > 0.0){
        timesperstep[i]++;
        enperstep[i] = enperstep[i] + energy;
    }
}

void RBSsimuNS::Incident::penetrationpoweraccumu(const int &nions) {
    /*
     Print the projectile energy * ions along the depth
     Note:
     The result should correspond to (depth + 0.5 * depth-bin)
     */

    int i;
    int nprint = 40;

    std::ofstream penpowerout;

    for (i = 0; i < numdepth; i++){
        if (timesperstep[i] != 0) totalenergyperstep[i] = totalenergyperstep[i] + enperstep[i] / timesperstep[i];
    }

    if ((nions + 1) % nprint == 0 || (nions + 1) == nincident){
        penpowerout.open("out/penetrativepower.dat", std::ios::out);
        if(!penpowerout){
            printf("Cannot open the out/penetrativepower.dat file\n");
        }
        else{
            for (i = 0; i < numdepth; i++){
                penpowerout << std::fixed << std::setw(10) << penetrationpowerstep * i << "  ";
                penpowerout << std::setw(10) << totalenergyperstep[i] << std::endl;
            }
            penpowerout.close();
        }
    }
}

void RBSsimuNS::Incident::print_details() {
    /*
     Print some details
     */

    float au;

    std::ofstream detailout;

    detailout.open("out/Details.dat", std::ios::out);
    if(!detailout){
        printf("Cannot open out/Details.dat file\n");
    }
    else{
        detailout << "==================================================================" << std::endl;
        detailout << "|                           RBSADEC                              |" << std::endl;
        detailout << "==================================================================\n" << std::endl;

        detailout << "Target average atomic density: " << std::scientific << std::setprecision(5);
        detailout << atom->ndensity*(1e24) << " atoms/cm3" << std::endl;
        detailout << "Maximum impact parameter (average atomic volume): ";
        detailout << target->rmax << " Angstrom\n" << std::endl;

        detailout << "Element  Count    Maximum_impact_parameter(Ang.)  Universal_screening_length(Ang.)  Therm_vibra_magni.(pm)" << std::endl;
        for (int i=0; i<atom->ntype; ++i){
            au = 0.8853 / (pow(z, 0.23) + pow(atom -> z[i], 0.23)) * 0.52917721092;
            detailout << std::setw(7) << std::left << atom->symbol[i] << "  ";
            detailout << atom->itypenum[i] << "    ";
            detailout << std::setw(23) << std::right << maximpact[i] << "  ";
            detailout << std::setw(32) << std::right << au << "  ";
            detailout << std::setw(24) << std::right << atom -> debyemagn[i] * 100 << std::endl;
        }
    }
}

void RBSsimuNS::Incident::printtime(const double &t1, const double &t2) {
    /*
     Print simulation time
     */

    std::ofstream timeout;

    timeout.open("out/Simu_time.dat", std::ios::out);
    if(!timeout){
        printf("Cannot open the out/Simu_time.dat file");
    }
    else{
        timeout << "Find partner spend " << std::fixed << t1 << " s" << std::endl;
        timeout << "All simu spend " << t2 << " s" << std::endl;
    }
}

void RBSsimuNS::Incident::max_impact_parameter()
{
    /*
     If the minimum potential is 0, still use the original maximum impact parameter (Rmax);
     Otherwise, calculate the maximum impact parameter at which the screened potential
     between the incident ion and the target atom equals to the minimum potential by
     the Newton's method.
     */

    double Vmini=parameter->minipot;
    double epsi=1e-10;
    int max_iteration=100;

    maximpact.resize(atom->ntype, 0);

    for (int i=0; i<atom->ntype; ++i)
    {
        if (Vmini == 0) {
            maximpact[i] = target->rmax;
        }
        else {
            maximpact[i]=Newton_method(target->rmax, z, atom -> z[i], Vmini, epsi, max_iteration);
        }
    }
}

double RBSsimuNS::Incident::Newton_method(double r0, const int z1, const int z2, const double V0, const double epsilon, const int max_iter)
{
    /*
     Use Newton's method to calculate the maximum impact parameter.
     epsilon: error;
     max_iter: maximum iteration number;
     a: the screening length;
     phiU: the universal screening potential; (SRIM tutorial, 2015, pp 2-32);
     fxn: the inter-atomic potential;
     Dfxn: the first order derivative of the inter-atomic potential.
     */

    double rn=r0;
    double phiU, phi_deriva, x, a;
    double _E2=14.3996445;
    double fxn, Dfxn;

    for(int i=0; i<max_iter; ++i){
        a=0.8853 / (pow(z1, 0.23) + pow(z2, 0.23)) * 0.52917721092;
        x=rn/a;
        phiU=0.1818*exp(-3.2*x)+0.5099*exp(-0.9423*x)+0.2802*exp(-0.4029*x)+0.02817*exp(-0.2016*x);
        fxn=(z1*z2*_E2)*phiU/rn-V0;

        if (fabs(fxn)<epsilon) return(rn);

        phi_deriva=0.1818*(-3.2)*exp(-3.2*x)+0.5099*(-0.9423)*exp(-0.9423*x)+\
        0.2802*(-0.4029)*exp(-0.4029*x)+0.02817*(-0.2016)*exp(-0.2016*x);
        Dfxn=(z1*z2*_E2*phi_deriva*x-z1*z2*_E2*phiU)/(rn*rn);
        if (Dfxn==0){
            printf("The maximum impact parameter calculation has problem for %d and %d.\n", z1, z2);
            return (target->rmax);
        }
        rn=rn-fxn/Dfxn;
    }
    printf("Exceeded maximum iterations: %d\n",max_iter);
    return(target->rmax);
}
