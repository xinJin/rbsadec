#include <iostream>
#include <string>
#include <chrono> 
#include <ctime> 
#include <cstdio> 
#include <memory>
#include <mpi.h>

#include "rbssimu.h"
#include "incident.h"
#include "parameter.h"
#include "atom_type.h"
#include "target.h"
#include "atom.h"

using namespace RBSsimuNS;

int main(int argc, char **argv) {

    int i;
    int rangeflag, rbsflag, rbscountflag, nuclossflag, recoilspectraflag, finalangleflag, penetrationpowerflag;
    int pathflag, locDenflag, locDenflag2, nraflag; //Abj

    std::string arg;

    rangeflag = 0;
    rbsflag = 0;
    rbscountflag = 0;
    nuclossflag = 0;
    recoilspectraflag = 0;
    finalangleflag = 0;
    penetrationpowerflag = 0;
    pathflag = 0;
    locDenflag = 0;
    locDenflag2 = 0;
    nraflag = 0;

    for (i = 1; i < argc; i++) {
        arg = argv[i];
        if (arg == "-h") {

            std::cerr << "Usage: " << argv[0] << " option(s) "
                      << std::endl 
                      << "Options: " << std::endl
                      << "-h          help\n"
                      << "-r          Range simulation\n"
                      << "-rbs        Rutherford Backscattering Spectra simulation \n"
                      << "-loc        Automatically take into account the local density variation (elec. energy loss) \n"
                      << "-loc2       Similar to '-loc', but works in fixed regions, current geometry: box, sphere \n"
                      << "-nra        Nuclear reaction analysis simulation"
                      << "-p          Output the encounter probabiltiy\n"
                      << "-nuc        Output the depth profile of nuclear energy loss \n"
                      << "-recoils    Output the recoil energy spectra \n"
                      << "-fa         Output the final angle distribution \n"
                      << "-pp         Output the penetration power of ions \n"
                      << "-path       Output the trajectory of ions (to be developed)\n"
                      << "Default:    -rbs" << std::endl;

            exit(0);
        }
        if (arg == "-r") rangeflag = 1;
        if (arg == "-rbs") rbsflag = 1;
        if (arg == "-p") rbscountflag = 1;
        if (arg == "-nuc") nuclossflag = 1;
        if (arg == "-recoils") recoilspectraflag = 1;
        if (arg == "-fa") finalangleflag = 1;
        if (arg == "-pp") penetrationpowerflag = 1;
        if (arg == "-path") pathflag = 1; //Abj
        if (arg == "-loc") locDenflag = 1; //Abj
        if (arg == "-loc2") locDenflag2 = 1; //AbJ
        if (arg == "-nra") nraflag = 1; //AbJ
    }
    if (1 == argc) rbsflag = 1;

    MPI_Init(NULL, NULL);

    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    if (0 == world_rank) {
        printf("\nCode name: %s (mpi distributed version)\n", argv[0]);
        printf("Using %d processors...\n\n", world_size);
    }

    std::unique_ptr<RBSsimu> rbssimu(new RBSsimu);

    rbssimu -> incident -> simu(rangeflag, rbsflag, rbscountflag, nuclossflag, recoilspectraflag, finalangleflag,
                                penetrationpowerflag, pathflag, locDenflag, locDenflag2, nraflag);

    //Added by JIN

    MPI_Barrier(MPI_COMM_WORLD);

    if (0 == world_rank) {
        auto end = std::chrono::system_clock::now();
        std::time_t end_time = std::chrono::system_clock::to_time_t(end);
        std::cout<<"\nComputation finished at: "<<std::ctime(&end_time)<<std::endl;
    }

    MPI_Finalize();
}
