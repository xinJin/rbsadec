#ifndef REBUILD_POINTER_H
#define REBUILD_POINTER_H

#include <memory>
#include "rbssimu.h"

namespace RBSsimuNS {

    class Pointer {
    public:
       
        class RBSsimu *rbssimu;

        //Important to set them as reference to pointer
        std::shared_ptr<class Incident>     &incident;
        std::shared_ptr<class Parameter>    &parameter;
        std::shared_ptr<class Atom_type>    &atom_type;
        std::shared_ptr<class Target>       &target;
        std::shared_ptr<class Atom>         &atom;
        std::shared_ptr<class NRA>          &nra;


        Pointer(RBSsimu *rbs) : rbssimu(rbs),
                                incident(rbs->incident),
                                parameter(rbs->parameter),
                                atom_type(rbs->atom_type),
                                target(rbs->target),
                                atom(rbs->atom),
                                nra(rbs->nra){
        }

        ~Pointer() {
        }
    };

}


#endif 
