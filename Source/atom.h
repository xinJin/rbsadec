#ifndef REBUILD_ATOM_H
#define REBUILD_ATOM_H

#include <string>
#include <vector>

#include "pointer.h"

namespace RBSsimuNS {
    class Atom: public Pointer {
    public:
        int                         ntype;
        std::vector<std::string>    symbol;
        std::vector<double>         vibrationmagn;

        std::vector<double>         mass, z, edisplacement;
        std::vector<int>            itypenum;
        std::vector<double>         debyemagn;
        int                         natotal;
        double                      meanz, ndensity;

        int                         nat;
        std::vector<int>            itype;

        std::vector<std::vector<double> >    scat_matrix, time_matrix;
        //std::vector<std::vector<double> >    x;
        double                      **x;

        double                      meanmass; //Added by JIN
        std::string                 CoordsPath; //Added by JIN
        std::string                 ScatterPath; //AbJ
        std::string                 TimePath; //AbJ

        int                         vibrat_mode; //AbJ

        Atom(RBSsimu *rbs);
        ~Atom();

        void init();
        void read_coordinate();
        void properties();
        void read_scatter_matrix();
        void read_time_matrix();

        //"**&": a reference to a pointer to a pointer
        template <class T> void allocate_2d(T **&array, int row, int col){
            int i, j;
            array = new T* [row];//creat an array with "row" rows
            array[0] = new T [row * col];//let the fist row have "col" columns
            for(i = 1; i < row; i++) array[i] = array[i-1] + col;//indicate the start position of the next row
        }
    };
}


#endif 
