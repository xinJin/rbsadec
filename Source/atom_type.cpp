#include <sstream> 
#include <fstream> 
#include <iostream> 

#include "atom_type.h"
#include "parameter.h"

using namespace RBSsimuNS;

RBSsimuNS::Atom_type::Atom_type(RBSsimu *rbs):Pointer(rbs){

    symbol.reset(new std::string[MAXTYPE]);
    z.reset(new double[MAXTYPE]);
    mass.reset(new double[MAXTYPE]);
    estop.reset(new double[MAXTYPE]);
    edisplacement.reset(new double[MAXTYPE]);
}

RBSsimuNS::Atom_type::~Atom_type() {
    
}

void RBSsimuNS::Atom_type::Atom_type_read() {
    const int num = 5, linenum = 150;
    int i, j;
    int index[num];
    std::string line[linenum];
    std::stringstream ss[linenum];
    std::string temp;
    std::ifstream read;

    for(i = 0; i < num; i++) index[i] = 0;

    read.open(TablePath);
    if(!read){
        std::cerr << "Uh oh," << TablePath <<  " could not be opened for reading!" << std::endl;
        exit(1);
    }
    else{
        i = 0;
        while(read){
            getline(read,line[i]);
            ss[i].str(line[i]);
            if(ss[i] >> temp){
                if(temp == "Symbol"){
                    while(ss[i] >> symbol[index[0]]){
                        index[0]++;

                        //if (index[0] == MAXTYPE) {
                        //    printf("Warning: the total number of elements/nucleus in the table may become bigger than the limit (%d), please consider to increase the MAXTYPE in atom_type.h\n", MAXTYPE);
                        //}

                    }
                }
                if(temp == "Z-man"){
                    while(ss[i] >> z[index[1]]){
                        index[1]++;
                    }
                }
                if(temp == "Mass"){
                    while(ss [i]>> mass[index[2]]){
                        index[2]++;
                    }
                }
                if(temp == "Ed"){
                    while(ss[i] >> edisplacement[index[3]]){
                        index[3]++;
                    }
                }
                if(temp == "Es"){
                    while(ss[i] >> estop[index[4]]){
                        index[4]++;
                    }
                }
            }
            i++;
            if(i > linenum){
                std::cerr << "The line number in table.dat is more than 150, decrease the line in table.dat or increase the number in atom_type.cpp, exit..." << std::endl;
                read.close();
                exit(1);
            }
        }
    }
    read.close();
}

int RBSsimuNS::Atom_type::value_atom_type(const std::string v_symbol, double &v_z, double &v_mass, double &v_edis,
                                          double &v_estop) {
    int i;
    int find = 0;
    for (i = 0; i < MAXTYPE; i++){
        if(0 == v_symbol.compare(symbol[i])){
            v_z = z[i];
            v_mass = mass[i];
            v_edis = edisplacement[i];
            v_estop = estop[i];
            find = 1;
            break;
        }
    }
    return find;
}
