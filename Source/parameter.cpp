#include <fstream> 
#include <iostream> 
#include <sstream>
#include <mpi.h>

#include "parameter.h"
#include "atom_type.h"
#include "atom.h"
#include "incident.h"
#include "rbssimu.h"
#include "target.h"

using namespace RBSsimuNS;

RBSsimuNS::Parameter::Parameter(RBSsimu *rbs):Pointer(rbs) {

    inputpath = "in/input.dat";
    locDenflag = 0;
    locDenflag2 = 0;
    ionNumberPerProc = 1;
    ionNumberPerProcOdd = 1;
}

RBSsimuNS::Parameter::~Parameter() {
    
}

void RBSsimuNS::Parameter::read_parameter() {
    int i;

    std::ifstream opinput;
    std::ostringstream buf;

    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    opinput.open(inputpath, std::ios::in);
    if(!opinput){
        std::cerr << "Uh oh," << inputpath <<  " could not be opened for reading!" << std::endl;
        exit(1);
    }

    readsc("Ion", ionsymbol, "He", "Incident ion: ", opinput);
    readdcf("Energy", energy, 2000000, "Energy of Incident Ion: ", opinput);
    readicf("Nhistory", numhis, 10000, "Number of Histroy: ", opinput);

    if (0 == world_rank){
        if (numhis < world_size) printf("Warning: the number of incident ions (%d) is smaller than the number of processors (%d).\n",
                                        numhis, world_size);
        int modul = numhis % world_size;
        ionNumberPerProc = (numhis - modul) / world_size + modul;
    }
    else {
        int modul = numhis % world_size;
        ionNumberPerProc = (numhis - modul) / world_size;
    }
    ionNumberPerProcOdd = (numhis - (numhis % world_size) ) / world_size;

    readdc("Theta.min", thetamin, 0, "Minimum Incident Angle Theta: ", opinput);
    readdc("Theta.max", thetamax, 0, "Maximum Incident Angle Theta: ", opinput);
    readdc("Fii.min", fiimin, 0, "Minimum Incident Angle Fii: ", opinput);
    readdc("Fii.max", fiimax, 0, "Maximum Incident Angle Fii: ", opinput);
    readdc("Rotation.theta", rotatheta, 0, "Rotation of Incident Angle theta: ", opinput);
    readdc("Rotation.fii", rotafii, 0, "Rotation of Incident Angle Fii: ", opinput);
    readic("Mode.rotate", incident->rotate_mode, 0, "Rotation mode: ", opinput);
    readic("Mode.ibd", incident->IBD_mode, 0, "IBD mode: ", opinput);
    readic("Mode.tbm", rbssimu -> TBM_mode, 0, "TBM mode: ", opinput);
    if (1 == rbssimu -> TBM_mode){
        readdc("Dechannel.theta", rbssimu -> DechannelAngle, 0.0, "Dechanneling threshold angle: ", opinput);
    }

    readic("DebyeDisp", debyeflag, 1, "Debye Displacement (0 no 1 yes): ", opinput);
    readic("Mode.vibrate", atom->vibrat_mode, 0, "Vibration mode: ", opinput);
    if(0 == atom->vibrat_mode || 2 == atom->vibrat_mode){
        readdc("DebyeT", debyetemperature, 645, "Debye Temperature of the Target: ", opinput);
        readdc("Temperature", temperature, 300.0, "Environment temperature: ", opinput);
    }
    readdc("Density", density, 2.32, "The Density of the Target: ", opinput);
    readic("Ntype", ntype, 1, "Number of Elements in Target: ", opinput);

    /*
    Note: The element number should start from 1,
          and be consistent with the appearance order of the atom symbol.
          Otherwise, a problem may occur in Atom::::properties().
     */
    for(i = 0; i < ntype; i++){
        std::string tempsymbol; buf.str("");
        buf << "Element." << i+1;
        readsc(buf.str(), tempsymbol, "Si", "Element of Target: ", opinput);
        atomsymbol.push_back(tempsymbol); 
    }
    if(1 == atom->vibrat_mode) {
        for (i = 0; i < ntype; i++) {
            double temp;
            buf.str("");
            buf << "Displacement." << i + 1;
            readdc(buf.str(), temp, 0.1, "Ther. vibra. magnitude: ", opinput);
            vibration.push_back(temp);
        }
    }

    readdcf("Mini.potential", minipot, 3, "Minimum int.atomic potential: ", opinput); //Added by JIN
    readdcf("Coll.offset", coffset, 0, "Collision offset: ", opinput); //Added by JIN
    readdcf("Free.distance", freedist, 100, "Free flight distance: ", opinput); //Added by JIN
    readdcf("Threshold.energy", ThresholdEnergy, 25, "Threshold energy (eV): ", opinput); //Added by JIN
    readic("Mode.FFP", rbssimu->ffp_mode, 0, "FFP mode: ", opinput);//Added by JIN
    readic("Mode.Impact", rbssimu->impact_mode, 0, "Impact parameter mode: ", opinput);//Added by JIN

    readdc("Positionmin.x", positionmin[0], 0, "Minimum Incident Position x: ", opinput);
    readdc("Positionmin.y", positionmin[1], 0, "Minimum Incident Position y: ", opinput);
    readdc("Positionmin.z", positionmin[2], -3, "Minimum Incident Position z: ", opinput);
    readdc("Positionmax.x", positionmax[0], 10, "Maximum Incident Position x: ", opinput);
    readdc("Positionmax.y", positionmax[1], 10, "Maximum Incident Position y: ", opinput);
    readdc("Positionmax.z", positionmax[2], -3, "Maximum Incident Position z: ", opinput);

    readdc("Box.xlow", xlo[0], 0, "Minimum boundary in x: ", opinput);
    readdc("Box.ylow", xlo[1], 0, "Minimum boundary in y: ", opinput);
    readdc("Box.zlow", xlo[2], 0, "Minimum boundary in z: ", opinput);
    readdc("Boxsize.x", boxsize[0], 16.29, "Box Size in x: ", opinput);
    readdc("Boxsize.y", boxsize[1], 16.29, "Box Size in y: ", opinput);
    readdc("Boxsize.z", boxsize[2], 1629.0, "Box Size in z: ", opinput);
    readic("Boundary.x", boundary[0], 1, "Boundary Condition in x: ", opinput);
    readic("Boundary.y", boundary[1], 1, "Boundary Condition in y: ", opinput);
    readic("Boundary.z", boundary[2], 0, "Boundary Condition in z: ", opinput);

    readdc("Binsize.x", binsize[0], 2.715, "Bin Size in x: ", opinput);
    readdc("Binsize.y", binsize[1], 2.715, "Bin Size in y: ", opinput);
    readdc("Binsize.z", binsize[2], 2.715, "Bin Size in z: ", opinput);
    if(1 == locDenflag){
        dbinsize.resize(3, 2.715);
        readdc("DBinsize.x", dbinsize[0], 2.715, "DBin Size in x: ", opinput);
        readdc("DBinsize.y", dbinsize[1], 2.715, "DBin Size in y: ", opinput);
        readdc("DBinsize.z", dbinsize[2], 2.715, "DBin Size in z: ", opinput);
    }

    readdcf("DepthStop", depthstop, 1e4, "Maximum Depth of simulation: ", opinput);
    readic("Elstop.mode", elstopmode, 0, "Energy Loss Straggle(0 no 1 yes ): ", opinput);
    readdc("SpreadAngle", spreada, 60, "Backscatter spread angle: ", opinput); //Added by JIN
    readic("Seed", seed, 2578, "Random Seed Number: ", opinput);

    readdc("Detector.theta", detectortheta, 155.0, "Angle Theta of the Dectector: ", opinput);
    readdc("Detector.fii", detectorfii, 20.0, "Angle Fii of the Detector: ", opinput);
    readdc("Detector.radius", detectorradius, 10.0, "Radius of the Detector (mm): ", opinput); //Modified by M. Sequeira
    readdc("Detector.distance", detectordistance, 5.0, "Detector-target distance (cm): ", opinput);
    readic("Nchannel", nchannel, 4096, "Number of Channel in Data Acquiring System: ", opinput);
    readdc("Detector.FWHM", detectorfwhm, 10.0, "Resolution of the Detector (keV): ", opinput);
    readdc("Detector.slope", detectorslope, 1.0, "Calibration-slop (keV/Channel): ", opinput);
    readdc("Detector.intercept", detectorintercept, 10.0, "Calibration-intercept (keV): ", opinput);
    readdcf("Depthsize", depthsize, 10000.0, "Depth size of Statistical Rbs ion: ", opinput);

    readsc("table.dat", atom_type->TablePath, "Data/table.dat", "Table: ", opinput); //Added by JIN
    readsc("coords1.in", atom->CoordsPath, "in/coords1.in", "Coords1: ", opinput); //Added by JIN
    readsc("scatter.in", atom->ScatterPath, "Data/Scatter.mat", "Scatter: ", opinput); //Added by JIN
    readsc("time.in", atom->TimePath, "Data/Time.mat", "Time: ", opinput); //Added by JIN
    readsc("elstop1.in", incident->stopPath, "in/elstop1.in", "Stop: ", opinput); //Added by JIN

    if(1 == locDenflag2){
        readic("Nbox", nbox, 1, "Number of Boxes in Target: ", opinput);
        for (i = 0; i < nbox; i++) {
            std::vector<double> locBoxTmp(8, 0);
            std::vector<double> getLocBox;
            buf.str("");
            buf << "Region.box." << i+1;
            std::string messTmp = "Local box info.";
            messTmp += std::to_string(i + 1);
            messTmp += ": ";
            readBox(buf.str(), getLocBox, locBoxTmp, messTmp, opinput);
            target->locBox.push_back(getLocBox);
        }

        readic("Nsphere", nsph, 1, "Number of Spheres in Target: ", opinput);
        for (i = 0; i < nsph; i++) {
            std::vector<double> locSphereTmp(6, 0);
            std::vector<double> getLocSph;
            buf.str("");
            buf << "Region.sph." << i+1;
            std::string messTmp = "Local sphere info.";
            messTmp += std::to_string(i + 1);
            messTmp += ": ";
            readSphere(buf.str(), getLocSph, locSphereTmp, messTmp, opinput);
            target->locSphere.push_back(getLocSph);
        }
    }

    if (1 == nraflag) { //adj
        readic("NRA.mode", nraReactionType, 0, "Reaction mode (NRA): ", opinput);
        readsc("NRA.nucleus", nucleusSymbol, "O16", "Reaction nucleus (NRA): ", opinput);
        readsc("NRA.emit", emitNucluesSymbol, "H1", "Emitted nucleus (NRA): ", opinput);
        readsc("NRA.residual", residualNucleusSymbol, "C12", "Residual nucleus (NRA): ", opinput);
        readdc("NRA.Q", nraQenergy, 0.0, "Reaction energy (NRA): ", opinput);
        readdc("NRA.sa", nraSpreadAngle, 0.0, "Spread angle (NRA): ", opinput);

        readsc("NRA.csection", crossSecPath, "in/diffCS.in", "Cross section (NRA): ", opinput);
        readsc("NRA.elstop", emitStopPowerPath, "in/elstop_nra.in", "Stop. power (NRA): ", opinput);
    }

    opinput.close();
}


//Only read the parameter after the keyword (varname) and "=", and each element should be separated by space.
void RBSsimuNS::Parameter::readsc(const std::string varname, std::string &v, const std::string v0,
                                  const std::string mess, std::ifstream &in) {
    in.clear();
    in.seekg(0, std::ios::beg);

    std::string line;
    std::stringstream ss;
    std::string temp1, temp2;

    int find = 0;

    while(in){
        getline(in, line);
        ss.str(line);
        if(ss >> temp1 && ss >> temp2){
            if (temp1 == varname && temp2 == "=") {
                ss >> v;
                find = 1;
                break;
            }
        }
        temp1.clear();
        temp2.clear();
        ss.clear(); //Clear the stringstream, in order to read the next line
    }

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    if (1 == find){
        if (0 == world_rank) printf("%s %s\n", mess.c_str(), v.c_str());
    }

    if (0 == find){
        if (0 == world_rank) printf("%s%s (default)\n", mess.c_str(), v0.c_str());
        v = v0;
    }

    in.clear();
    in.seekg(0, std::ios::beg);
}

void RBSsimuNS::Parameter::readic(const std::string varname, int &v, const int v0, const std::string mess,
                                  std::ifstream &in) {
    in.clear();
    in.seekg(0, std::ios::beg);

    std::string line;
    std::stringstream ss;
    std::string temp1, temp2;

    int find = 0;

    while(in){
        getline(in, line);
        ss.str(line);
        if(ss >> temp1 && ss >> temp2){
            if (temp1 == varname && temp2 == "=") {
                ss >> v;
                find = 1;
                break;
            }
        }
        temp1.clear();
        temp2.clear();
        ss.clear(); 
    }

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    if (1 == find){
        if (0 == world_rank) printf("%s%d\n", mess.c_str(), v);
    }

    if (0 == find){
        if (0 == world_rank) printf("%s%d (default)\n", mess.c_str(), v0);
        v = v0;
    }

    in.clear();
    in.seekg(0, std::ios::beg);
}

void RBSsimuNS::Parameter::readicf(const std::string varname, int &v, const int v0, const std::string mess,
                                  std::ifstream &in) {
    in.clear();
    in.seekg(0, std::ios::beg);

    std::string line;
    std::stringstream ss;
    std::string temp1, temp2;

    int find = 0;

    while(in){
        getline(in, line);
        ss.str(line);
        if(ss >> temp1 && ss >> temp2){
            if (temp1 == varname && temp2 == "=") {
                ss >> v;
                find = 1;
                break;
            }
        }
        temp1.clear();
        temp2.clear();
        ss.clear(); 
    }

    double vv = v, vv0 = v0;

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    if (1 == find){
        if (0 == world_rank) printf("%s%.3e\n", mess.c_str(), vv);
    }

    if (0 == find){
        if (0 == world_rank) printf("%s%.3e (default)\n", mess.c_str(), vv0);
        v = v0;
    }

    in.clear();
    in.seekg(0, std::ios::beg);
}

void RBSsimuNS::Parameter::readdc(const std::string varname, double &v, const double v0, const std::string mess,
                                   std::ifstream &in) {
    in.clear();
    in.seekg(0, std::ios::beg);

    std::string line;
    std::stringstream ss;
    std::string temp1, temp2;

    int find = 0;

    while(in){
        getline(in, line);
        ss.str(line);
        if(ss >> temp1 && ss >> temp2){
            if (temp1 == varname && temp2 == "=") {
                ss >> v;
                find = 1;
                break;
            }
        }
        temp1.clear();
        temp2.clear();
        ss.clear(); 
    }

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    if (1 == find){
        if (0 == world_rank) printf("%s%.3f\n", mess.c_str(), v);
    }

    if (0 == find){
        if (0 == world_rank) printf("%s%.3f (default)\n", mess.c_str(), v0);
        v = v0;
    }

    in.clear();
    in.seekg(0, std::ios::beg);
}

void RBSsimuNS::Parameter::readdcf(const std::string varname, double &v, const double v0, const std::string mess,
                                  std::ifstream &in) {
    in.clear();
    in.seekg(0, std::ios::beg);

    std::string line;
    std::stringstream ss;
    std::string temp1, temp2;

    int find = 0;

    while(in){
        getline(in, line);
        ss.str(line);
        if(ss >> temp1 && ss >> temp2){
            if (temp1 == varname && temp2 == "=") {
                ss >> v;
                find = 1;
                break;
            }
        }
        temp1.clear();
        temp2.clear();
        ss.clear(); 
    }

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    if (1 == find){
        if (0 == world_rank) printf("%s%.3e\n", mess.c_str(), v);
    }

    if (0 == find){
        if (0 == world_rank)printf("%s%.3e (default)\n", mess.c_str(), v0);
        v = v0;
    }

    in.clear();
    in.seekg(0, std::ios::beg);

}

void RBSsimuNS::Parameter::readBox(const std::string varname, std::vector<double> &v, const std::vector<double> v0,
                                   const std::string mess, std::ifstream &in) {
    /*
    A local density variation domain with a box shape.
    Parameter: xlow, xhigh, ylow, yhigh, zlow, zhigh,
               local density [g/cm^3] and local atomic density [n/cm^3]
     */

    in.clear();
    in.seekg(0, std::ios::beg);

    std::string line;
    std::stringstream ss;
    std::string temp1, temp2;
    double tmpSize;

    int find = 0;

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    while(in){
        getline(in, line);
        ss.str(line);
        if(ss >> temp1 && ss >> temp2){
            if (temp1 == varname && temp2 == "=") {

                for (int i; i < 8; i++) {

                    if (ss >> tmpSize) {
                        v.push_back(tmpSize);
                    }
                    else if (0 == i) {
                        if (0 == world_rank) printf("xlow is missing in the Region.box command\n");
                        in.close();
                        exit(1);
                    }
                    else if (1 == i) {
                        if (0 == world_rank) printf("xhigh is missing in the Region.box command\n");
                        in.close();
                        exit(1);
                    }
                    else if (2 == i) {
                        if (0 == world_rank) printf("ylow is missing in the Region.box command\n");
                        in.close();
                        exit(1);
                    }
                    else if (3 == i) {
                        if (0 == world_rank) printf("ymax is missing in the Region.box command\n");
                        in.close();
                        exit(1);
                    }
                    else if (4 == i) {
                        if (0 == world_rank) printf("zlow is missing in the Region.box command\n");
                        in.close();
                        exit(1);
                    }
                    else if (5 == i) {
                        if (0 == world_rank) printf("zhigh is missing in the Region.box command\n");
                        in.close();
                        exit(1);
                    }
                    else if (6 == i) {
                        if (0 == world_rank) printf("Density is missing in the Region.box command\n");
                        in.close();
                        exit(1);
                    }
                    else if (7 == i) {
                        if (0 == world_rank) printf("Atomic density is missing in the Region.box command\n");
                        in.close();
                        exit(1);
                    }
                }

                find = 1;
                break;
            }
        }

        temp1.clear();
        temp2.clear();
        ss.clear();
    }

    if (1 == find){
        if (0 == world_rank) {
            printf("%s", mess.c_str());
            for (int i; i < v.size(); i++) printf("%.3f ", v[i]);
            printf("\n");
        }
        v[7] = v[7] * (1e-24); //Convert the unit of atomic density to [n/A^3]
    }

    if (0 == find) {
        if (0 == world_rank) {
            printf("%s", mess.c_str());
            for (int i; i < v0.size(); i++) printf("%.3f ", v0[i]);
            printf("(default)\n");
        }
        v = v0;
    }

    in.clear();
    in.seekg(0, std::ios::beg);
}

void RBSsimuNS::Parameter::readSphere(const std::string varname, std::vector<double> &v, const std::vector<double> v0,
                                      const std::string mess, std::ifstream &in) {
    /*
    A local density variation domain with a sphere shape.
    Parameter: x_center, y_center, z_center, radius,
               local density [g/cm^3] and local atomic density [n/cm^3]
     */

    in.clear();
    in.seekg(0, std::ios::beg);

    std::string line;
    std::stringstream ss;
    std::string temp1, temp2;
    double tmpSize;

    int find = 0;

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    while(in){
        getline(in, line);
        ss.str(line);
        if(ss >> temp1 && ss >> temp2){
            if (temp1 == varname && temp2 == "=") {

                for (int i; i < 6; i++) {

                    if (ss >> tmpSize) {
                        v.push_back(tmpSize);
                    }
                    else if (0 == i) {
                        if (0 == world_rank) printf("x_center is missing in the Region.sphere command\n");
                        in.close();
                        exit(1);
                    }
                    else if (1 == i) {
                        if (0 == world_rank) printf("y_center is missing in the Region.sphere command\n");
                        in.close();
                        exit(1);
                    }
                    else if (2 == i) {
                        if (0 == world_rank) printf("z_center is missing in the Region.sphere command\n");
                        in.close();
                        exit(1);
                    }
                    else if (3 == i) {
                        if (0 == world_rank) printf("radius is missing in the Region.sphere command\n");
                        in.close();
                        exit(1);
                    }
                    else if (4 == i) {
                        if (0 == world_rank) printf("Density is missing in the Region.sphere command\n");
                        in.close();
                        exit(1);
                    }
                    else if (5 == i) {
                        if (0 == world_rank) printf("Atomic density is missing in the Region.sphere command\n");
                        in.close();
                        exit(1);
                    }
                }

                find = 1;
                break;
            }
        }

        temp1.clear();
        temp2.clear();
        ss.clear();
    }

    if (1 == find){
        if (0 == world_rank) {
            printf("%s", mess.c_str());
            for (int i; i < v.size(); i++) printf("%.3f ", v[i]);
            printf("\n");
        }
        v[5] = v[5] * (1e-24); //Convert the unit of atomic density to [n/A^3]
    }

    if (0 == find) {
        if (0 == world_rank) {
            printf("%s", mess.c_str());
            for (int i; i < v0.size(); i++) printf("%.3f ", v0[i]);
            printf("(default)\n");
        }
        v = v0;
    }

    in.clear();
    in.seekg(0, std::ios::beg);
}
